<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Darah_model extends CI_Model {

	public function get_komponen()
	{
		return $this->db->select('komponen_id, komponen_nama')
						->from('komponen_darah')
						->get()->result();
	}

}

/* End of file Darah_model.php */
/* Location: ./application/models/Darah_model.php */