<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemasok_model extends CI_Model {

	public function get_pemasok()
	{
		return $this->db->select('pemasok_id, pemasok_nama')
					->from('pemasok')
					->get()->result();
	}

}

/* End of file Pemasok_Model.php */
/* Location: ./application/models/Pemasok_Model.php */