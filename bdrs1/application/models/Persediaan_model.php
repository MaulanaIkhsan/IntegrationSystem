<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persediaan_model extends CI_Model {

	/*------------------- Input Persediaan Darah -------------------*/

	//mengambil "komponenen_id' dari tabel "komponen_darah" berdasarkan "komponen_nama"
	public function get_komponen_id($komponen_simbol)
	{
		$query = $this->db->select('komponen_id')
						->from('komponen_darah')
						->where('komponen_simbol', $komponen_simbol)
						->get()->row();

		return $query->komponen_id;
	}

	//mengambil "golongan_id' dari tabel "golongan_darah" berdasarkan "golongan_nama"
	public function get_golongan_id($golongan_nama)
	{
		$query = $this->db->select('golongan_id')
						->from('golongan_darah')
						->where('golongan_nama', $golongan_nama)
						->get()->row();

		return $query->golongan_id;
	}

	//mengambil "labu_id' dari tabel "labu" berdasarkan "labu_jenis"
	public function get_labu_id($labu_jenis)
	{
		$query = $this->db->select('labu_id')
						->from('labu')
						->where('labu_jenis', $labu_jenis)
						->get()->row();

		return $query->labu_id;
	}

	//mengambil "masuk_id' dari tabel "darah_masuk" berdasarkan "komponen_id" dan "golongan_id"
	public function get_masuk_id($komponen_id, $golongan_id, $permintaan_kode)
	{
		$id_masuk = $this->db->select('masuk_id')
							->from('darah_masuk')
							->where('komponen_id', $komponen_id)
							->where('golongan_id', $golongan_id)
							->where('permintaan_kode', $permintaan_kode)
							->where('masuk_tgl', date('Y-m-d'))
							->get()->row();
		
		if($id_masuk == NULL) {
			return 'kosong';
		}
		else {
			return $id_masuk->masuk_id;
		}
		
	}

	//mengambil jumlah total pada tabel "darah masuk"
	public function get_total_darah_masuk($permintaan_kode, $komponen_id, $golongan_id)
	{
		$query = $this->db->select('masuk_total')
						->from('darah_masuk')
						->where('permintaan_kode', $permintaan_kode)
						->where('komponen_id', $komponen_id)
						->where('golongan_id', $golongan_id)
						->where('masuk_tgl', date('Y-m-d'))
						->get()->row();

		return $query->masuk_total;
	}

	//mengambil jumlah total pada tabel "masuk_perkantung"
	public function get_total_masuk_perkantung($masuk_id, $labu_id)
	{
		$query = $this->db->select('perkantung_total as total_masuk_perkantung')
						->from('masuk_perkantung')
						->where('masuk_id', $masuk_id)
						->where('labu_id', $labu_id)
						->get()->row();

		return $query->total_masuk_perkantung;
	}

	//mengambil jumlah total pada tabel "jml_persediaan"
	public function get_total_jml_persediaan($komponen_id, $golongan_id)
	{
		$query = $this->db->select('jmlpersediaan_total')
						->from('jml_persediaan')
						->where('komponen_id', $komponen_id)
						->where('golongan_id', $golongan_id)
						->get()->row();

		return $query->jmlpersediaan_total;
	}

	public function get_total_persediaan_perkantung($jmlpersediaan_id,$labu_id)
	{
		$query = $this->db->select('perkantung_total')
						->from('persediaan_perkantung')
						->where('jmlpersediaan_id', $jmlpersediaan_id)
						->where('labu_id', $labu_id)
						->get()->row();

		return $query->perkantung_total;
	}

	public function get_jml_persediaan_id($komponen_id, $golongan_id)
	{
		$query = $this->db->select('jmlpersediaan_id')
						->from('jml_persediaan')
						->where('komponen_id', $komponen_id)
						->where('golongan_id', $golongan_id)
						->get()->row();

		return $query->jmlpersediaan_id;
	}

	//mengecek jumlan log "darah_masuk" hari ini berdasarkan "komponen_id" dan "golongan_id"
	public function check_darah_masuk($komponen_id, $golongan_id, $permintaan_kode)
	{
		$check_data = $this->db->select('count(masuk_id) as jumlah')
							->from('darah_masuk')
							->where('komponen_id', $komponen_id)
							->where('golongan_id', $golongan_id)
							->where('masuk_tgl', date('Y-m-d'))
							->where('permintaan_kode', $permintaan_kode)
							->get()->row();

		if ($check_data->jumlah >= "1") {
			return "ada";
		}
		else if ($check_data->jumlah == "0") {
			return "baru";
		}
		else {
			return "error";
		}

		// return $check_data->jumlah;
	}

	//mengecek jumlah log "masuk_perkantung" hari ini berdasarkan "komponen_id", "golongan_id" dan 
	public function check_masuk_perkantung($komponen_id, $golongan_id, $labu_id, $permintaan_kode)
	{
		$masuk_id = $this->get_masuk_id($komponen_id, $golongan_id, $permintaan_kode);
		if ($masuk_id == '0') {
			return "baru";
		}
		else {
			$check_data = $this->db->select('count(perkantung_id) as jumlah_perkantung')
								->from('masuk_perkantung')
								->where('labu_id', $labu_id)
								->where('masuk_id', $masuk_id)
								->get()->row();
			if ($check_data->jumlah_perkantung >= "1") {
				return "ada";
			}
			else if($check_data->jumlah_perkantung == "0") {
				return "baru";
			}
			else {
				return "error";
			}
		}
		
	
	}

	public function check_permintaan_kode($permintaan_kode)
	{
		$check_data = $this->db->select('count(permintaan_id) as id')
							->from('permintaan')
							->where('permintaan_kode', $permintaan_kode)
							->get()->row();
		
		if ($check_data->id == "1") {
			return "ada";
		}
		else {
			return "kosong";
		}

	}

	//mengupdate tabel "jml_persediaan" berdasarkan "komponen_id" dan "golongan_id"
	public function update_add_jml_persediaan($komponen_id, $golongan_id)
	{
		//menghitung total data yg masuk
		$query_total 		= $this->get_total_jml_persediaan($komponen_id, $golongan_id);
		$total 				= $query_total + 1;

		$data_update = array('jmlpersediaan_total' => $total);
		$kondisi 	 = array(
							'komponen_id' => $komponen_id,
							'golongan_id' => $golongan_id
						);
		
		$update_total = $this->db->where($kondisi)
								->update('jml_persediaan',$data_update);
		if ($update_total) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	//mengupdate tabel "persediaan_perkantung" berdasarkan "komponen_id" dan "golongan_id"
	public function update_add_persediaan_perkantung($komponen_id, $golongan_id, $labu_id)
	{
		
		$jml_persediaan_id 	= $this->get_jml_persediaan_id($komponen_id, $golongan_id);
		
		//menghitung total data yg masuk
		$query_total 		= $this->get_total_persediaan_perkantung($jml_persediaan_id, $labu_id);
		$total 				= $query_total + 1;

		$data_update 		= array('perkantung_total' => $total);
		$kondisi			= array(
									'jmlpersediaan_id' 	=> $jml_persediaan_id,
									'labu_id'			=> $labu_id
								);

		$update = $this->db->where($kondisi)
						->update('persediaan_perkantung', $data_update);
		if ($update) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	//menambahkan log baru pada "darah_masuk"
	public function new_log_darah_masuk($permintaan_kode, $komponen_id, $golongan_id)
	{
		$data_darah_masuk = array(
								'masuk_id' 			=> '',
								'komponen_id' 		=> $komponen_id,
								'golongan_id' 		=> $golongan_id,
								'masuk_tgl' 		=> date('Y-m-d'),
								'masuk_total' 		=> '1',
								'permintaan_kode'	=> $permintaan_kode
							);
		
		$add_log = $this->db->insert('darah_masuk', $data_darah_masuk);
		
		if ($add_log) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	//menambahkan log baru pada "masuk_perkantung"
	public function new_log_masuk_perkantung($permintaan_kode, $labu_id, $komponen_id, $golongan_id)
	{
		$darah_masuk_id = $this->get_masuk_id($komponen_id, $golongan_id, $permintaan_kode);
		
		//jika masih kosong
		if ($darah_masuk_id == '0') {
			$darah_masuk_id = '1';
		}

		$data_masuk_perkantung = array(
									'perkantung_id' 	=> '',
									'masuk_id' 			=> $darah_masuk_id,
									'labu_id' 			=> $labu_id,
									'perkantung_total' 	=> '1'
								);

		$add_log = $this->db->insert('masuk_perkantung', $data_masuk_perkantung);
		
		if ($add_log) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	//update log pada "darah_masuk"
	public function update_log_darah_masuk($permintaan_kode, $komponen_id, $golongan_id)
	{
		$query_total 	= $this->get_total_darah_masuk($permintaan_kode, $komponen_id, $golongan_id);
		$total 			= $query_total + 1;
		
		$data_darah_masuk 	= array('masuk_total' => $total);
		$kondisi 			= array(
								'komponen_id' 		=> $komponen_id, 
								'golongan_id' 		=> $golongan_id,
								'permintaan_kode' 	=> $permintaan_kode,
								'masuk_tgl'			=> date('Y-m-d')
							);
		
		$update_log = $this->db->where($kondisi)
								->update('darah_masuk', $data_darah_masuk);
		
		if ($update_log) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	//update log pada "masuk_perkantung"
	public function update_log_masuk_perkantung($permintaan_kode, $labu_id, $komponen_id, $golongan_id)
	{
		//mengambil 'id' dari tabel 'darah_masuk'
		$darah_masuk_id 	= $this->get_masuk_id($komponen_id, $golongan_id, $permintaan_kode);
		
		//menghitung total data yg masuk
		$query_total 		= $this->get_total_masuk_perkantung($darah_masuk_id, $labu_id);
		$total 				= $query_total + 1;
		
		//mengatur update
		$perkantung_total 	= array('perkantung_total' => $total);
		$kondisi			= array(
									'labu_id' 	=> $labu_id,
									'masuk_id'	=> $darah_masuk_id
								);
		
		$update_log = $this->db->where($kondisi)
								->update('masuk_perkantung', $perkantung_total);
		
		if ($update_log) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	//menambahkan data darah tabel "persediaan" 
	public function insert_persediaan($data_darah_masuk = array())
	{
		$komponen_id 		= $data_darah_masuk['komponen_id'];
		$golongan_id 		= $data_darah_masuk['golongan_id'];
		$labu_id 	 		= $data_darah_masuk['labu_id'];
		$permintaan_kode 	= $data_darah_masuk['permintaan_kode'];

		//mengambil nilai checking
		$check_permintaan_kode 	= $this->persediaan->check_permintaan_kode($data_darah_masuk['permintaan_kode']);
		$check_darah_masuk 		= $this->persediaan->check_darah_masuk($komponen_id, $golongan_id, $data_darah_masuk['permintaan_kode']);
		$check_masuk_perkantung = $this->persediaan->check_masuk_perkantung($komponen_id, $golongan_id, $labu_id, $data_darah_masuk['permintaan_kode']);


		$this->db->trans_begin();

		//pengecekan tipe data
		if (is_array($data_darah_masuk)) {

			//membuat log baru "darah masuk" dan "masuk_perkantung"
			if (($check_permintaan_kode == "ada") && ($check_darah_masuk == "baru") && ($check_masuk_perkantung == "baru")) {
				
				//menambahkan log "darah_masuk"
				$log_darah_masuk = $this->new_log_darah_masuk($permintaan_kode, $komponen_id, $golongan_id);
				
				if ($log_darah_masuk == 'ok') {
					
					//menambahkan log "masuk_perkantung"
					$log_masuk_perkantung = $this->new_log_masuk_perkantung($permintaan_kode, $labu_id, $komponen_id, $golongan_id);

					if ($log_masuk_perkantung == 'ok') {
						
						//update jml_persediaan
						$log_jml_persediaan = $this->update_add_jml_persediaan($komponen_id, $golongan_id);
						
						if ($log_jml_persediaan == 'ok') {
							
							//update persediaan_perkantung
							$log_persediaan_perkantung = $this->update_add_persediaan_perkantung($komponen_id, $golongan_id, $labu_id);
							
							if ($log_persediaan_perkantung == 'ok') {

								//menambahkan data darah ke "persediaan"
								$add_persediaan = $this->db->insert('persediaan', $data_darah_masuk);
								
								if ($add_persediaan) {
									$this->db->trans_commit();
									return 'ok';
								}
								else {
									$this->db->trans_rollback();
									return 'gagal ditambahkan';	
								}
								// $this->db->trans_commit();
								// return 'log darah_masuk berhasil dibuat, log masuk_perkantung berhasil dibuat, log jml_persediaan berhasil diupdate, log persediaan_perkantung berhasil diupdate';

							}
							else {
								
								$this->db->trans_rollback();
								return 'log persediaan_perkatung gagal dibuat';
							}
						}
						else {
							$this->db->trans_rollback();
							return 'log jml_persediaan gagal dibuat';
						}

					}
					else {
						$this->db->trans_rollback();
						return 'log masuk_perkantung gagal dibuat';
					}
				}
				else {
					$this->db->trans_rollback();
					return 'log darah_masuk gagal dibuat';
				}
			}

			//mengupdate log lama "darah masuk" dan "masuk_perkantung"
			else if(($check_permintaan_kode == "ada") && ($check_darah_masuk == "ada") && ($check_masuk_perkantung == "ada")) {
				
				//mengupdate log "darah_masuk"
				$log_darah_masuk = $this->update_log_darah_masuk($permintaan_kode, $komponen_id, $golongan_id);
				
				if ($log_darah_masuk == 'ok') {
					
					//mengupdate log "masuk_perkantung"
					$log_masuk_perkantung = $this->update_log_masuk_perkantung($permintaan_kode, $labu_id, $komponen_id, $golongan_id);

					if ($log_masuk_perkantung == 'ok') {
						
						//update jml_persediaan
						$log_jml_persediaan = $this->update_add_jml_persediaan($komponen_id, $golongan_id);
						
						if ($log_jml_persediaan == 'ok') {
							
							//update persediaan_perkantung
							$log_persediaan_perkantung = $this->update_add_persediaan_perkantung($komponen_id, $golongan_id, $labu_id);
							
							if ($log_persediaan_perkantung == 'ok') {

								//menambahkan data darah ke "persediaan"
								$add_persediaan = $this->db->insert('persediaan', $data_darah_masuk);
								
								if ($add_persediaan) {
									$this->db->trans_commit();
									return 'ok';
								}
								else {
									$this->db->trans_rollback();
									return 'gagal ditambahkan';	
								}
								// $this->db->trans_commit();
								// return 'log darah_masuk berhasil diupdate, log masuk_perkantung berhasil diupdate, log jml_persediaan berhasil diupdate, log persediaan_perkantung berhasil diupdate';
							}
							else {
								
								$this->db->trans_rollback();
								return 'log persediaan_perkatung gagal dibuat';
							}
						}
						else {
							$this->db->trans_rollback();
							return 'log jml_persediaan gagal dibuat';
						}

					}
					else {
						$this->db->trans_rollback();
						return 'log masuk_perkantung gagal dibuat';
					}
				}
				else {
					$this->db->trans_rollback();
					return 'log darah_masuk gagal dibuat';
				}
			}

			//mengupdate log lama "darah masuk" dan "masuk_perkantung"
			else if(($check_permintaan_kode == "ada") && ($check_darah_masuk == "ada") && ($check_masuk_perkantung == "baru")) {
				//mengupdate log "darah_masuk"
				$log_darah_masuk = $this->update_log_darah_masuk($permintaan_kode, $komponen_id, $golongan_id);
				
				if ($log_darah_masuk == 'ok') {
					
					//membuat log "masuk_perkantung" baru
					$log_masuk_perkantung = $this->new_log_masuk_perkantung($permintaan_kode, $labu_id, $komponen_id, $golongan_id);

					if ($log_masuk_perkantung == 'ok') {
						
						//update jml_persediaan
						$log_jml_persediaan = $this->update_add_jml_persediaan($komponen_id, $golongan_id);
						
						if ($log_jml_persediaan == 'ok') {
							
							//update persediaan_perkantung
							$log_persediaan_perkantung = $this->update_add_persediaan_perkantung($komponen_id, $golongan_id, $labu_id);
							
							if ($log_persediaan_perkantung == 'ok') {

								//menambahkan data darah ke "persediaan"
								$add_persediaan = $this->db->insert('persediaan', $data_darah_masuk);
								
								if ($add_persediaan) {
									$this->db->trans_commit();
									return 'ok';
								}
								else {
									$this->db->trans_rollback();
									return 'gagal ditambahkan';	
								}
								// $this->db->trans_commit();
								// return 'log darah_masuk berhasil diupdate, log masuk_perkantung berhasil dibuat, log jml_persediaan berhasil diupdate, log persediaan_perkantung berhasil diupdate';
							}
							else {
								
								$this->db->trans_rollback();
								return 'log persediaan_perkatung gagal dibuat';
							}
						}
						else {
							$this->db->trans_rollback();
							return 'log jml_persediaan gagal dibuat';
						}

					}
					else {
						$this->db->trans_rollback();
						return 'log masuk_perkantung gagal dibuat';
					}
				}
				else {
					$this->db->trans_rollback();
					return 'log darah_masuk gagal dibuat';
				}
			}

			//jika tidak memenuhi semua kriteria pada saat input data persediaan
			else {
				return 'proses penambahan persediaan gagal dilakukan';
			}
		}
		else {
			return 'data darah harus berupa array';
		}
	}	

	/*--------------------------------------------------------------*/

	/*------------------- Menampilkan Data Persediaan Darah -------------------*/
	public function get_jml_persediaan()
	{
		$query = "select 
						persediaan_total.golongan_darah as golongan_darah,
						jml_single.jumlah_single as total_single,
						jml_double.jumlah_double as total_double,
						persediaan_total.total as total_semuanya,
						persediaan_total.status_persediaan as status_persediaan
					from
						(
							select 
								golongan_darah.golongan_nama as golongan_darah,
								jml_persediaan.jmlpersediaan_id as persediaan_id,
								jmlpersediaan_total as total,
								(case 
									when jmlpersediaan_total <= get_batas_minimal_persediaan(komponen_darah.komponen_simbol, golongan_darah.golongan_nama) then 'Gawat'
									when jmlpersediaan_total >= get_batas_aman_persediaan(komponen_darah.komponen_simbol, golongan_darah.golongan_nama) then 'Aman'
									when 
										(jmlpersediaan_total > get_batas_minimal_persediaan(komponen_darah.komponen_simbol, golongan_darah.golongan_nama)) 
											&& 
										(jmlpersediaan_total < get_batas_aman_persediaan(komponen_darah.komponen_simbol, golongan_darah.golongan_nama)) 
										then 
											'Waspada'
								end
								) as status_persediaan
							from 
								jml_persediaan
									inner join
								golongan_darah on (golongan_darah.golongan_id = jml_persediaan.golongan_id)
									inner join
								komponen_darah on (komponen_darah.komponen_id = jml_persediaan.komponen_id)
							where
								komponen_darah.komponen_simbol = 'WB'
						) as persediaan_total
							inner join
						(
							select
								persediaan_perkantung.jmlpersediaan_id as persediaan_id, 
								perkantung_total as jumlah_single
							from
								persediaan_perkantung
									inner join
								labu on (persediaan_perkantung.labu_id = labu.labu_id)
									inner join
								jml_persediaan on (jml_persediaan.jmlpersediaan_id = persediaan_perkantung.jmlpersediaan_id)
									inner join
								komponen_darah on (komponen_darah.komponen_id = jml_persediaan.komponen_id)
							where
								labu.labu_jenis = 'Single' and
								komponen_darah.komponen_simbol = 'WB'
						) as jml_single
								
								on(persediaan_total.persediaan_id = jml_single.persediaan_id)
							
							inner join 
						(
							select
								persediaan_perkantung.jmlpersediaan_id as persediaan_id, 
								perkantung_total as jumlah_double
							from
								persediaan_perkantung
									inner join
								labu on(persediaan_perkantung.labu_id = labu.labu_id)
									inner join
								jml_persediaan on (jml_persediaan.jmlpersediaan_id = persediaan_perkantung.jmlpersediaan_id)
									inner join
								komponen_darah on (komponen_darah.komponen_id = jml_persediaan.komponen_id)
							where
								labu.labu_jenis = 'Double' and
								komponen_darah.komponen_simbol = 'WB'
						) as jml_double
								on(persediaan_total.persediaan_id = jml_double.persediaan_id)";

		$run = $this->db->query($query);
		
		return $run->result();
	}
	/*-------------------------------------------------------------------------*/

	public function get_detail_persediaan($golongan_darah, $jenis_kantung)
	{
		return $this->db->select('persediaan_barcode, persediaan_tgl_aftap, komponen_simbol, labu_jenis, golongan_nama, persediaan_rhesus, persediaan_tgl_kadaluwarsa, permintaan_kode, (DATEDIFF(persediaan_tgl_kadaluwarsa, curdate())) as umur')
						->from('persediaan')
						->join('komponen_darah', 'komponen_darah.komponen_id = persediaan.komponen_id', 'inner')
						->join('golongan_darah', 'golongan_darah.golongan_id = persediaan.golongan_id', 'inner')
						->join('labu', 'labu.labu_id = persediaan.labu_id', 'inner')
						->where('golongan_nama', $golongan_darah)
						->where('labu_jenis', $jenis_kantung)
						->order_by('umur, persediaan_barcode', 'desc')
						->get()->result();
	}

	//mengambil batas_interval_persediaan dari tabel "batas_persediaan" berdasarkan "komponen_id" dan "golongan_id"
	public function get_batas_interval_persediaan($komponen_id, $golongan_id)
	{
		return $this->db->select('batas_interval')
						->from('batas_persediaan')
						->where('komponen_id', $komponen_id)
						->where('golongan_id', $golongan_id)
						->get()
						->row()
						->batas_interval;
	}

	//mengambil "batas_aman" & "batas_minimal" dari tabel "batas_persediaan" berdasarkan "komponen_id" & "golongan_id"
	public function get_batas_persediaan($komponen_id, $golongan_id)
	{
		return $this->db->select('batas_aman, batas_minimal')
						->from('batas_persediaan')
						->where('komponen_id', $komponen_id)
						->where('golongan_id', $golongan_id)
						->get()->row();
	}

	//mengatur status persediaan di tabel "jml_persediaan" berdasarkan "komponen_id" & "golongan_id"
	public function set_status_persediaan($komponen_id, $golongan_id)
	{
		//mengambil jumlah persediaan
		$jml_persediaan_total 	= $this->get_total_jml_persediaan($komponen_id, $golongan_id);
		
		//mengambil batas persediaan
		$batas_persediaan 		= $this->get_batas_persediaan($komponen_id, $golongan_id);
		
		$batas_aman			= $batas_persediaan->batas_aman;
		$batas_minimal		= $batas_persediaan->batas_minimal;
		$status_persediaan 	= '';

		if ($jml_persediaan_total >= $batas_aman) {
			$status = 'Aman';
			return $status;
		}
		elseif ($jml_persediaan_total <= $batas_minimal) {
			$status = 'Gawat';
			return $status;
		}
		elseif (($jml_persediaan_total < $batas_aman) && ($jmlpersediaan_total > $batas_minimal)) {
			$status = 'Waspada';
			return $status;
		}
		else {
			$status = 'error';
			return $status;
		}
	}

	//untuk mengupdate interval persediaan, meliputi "tgl_interval_awal", "tgl_interval_akhir" 
	// & "status_persediaan" berdasarkan "komponen_id" & "golongan_id"
	public function update_interval_persediaan($komponen_id, $golongan_id)
	{
		$batas_interval = $this->get_batas_interval_persediaan($komponen_id, $golongan_id);

		$date_format 	= 'Y-m-d';
		$tgl_flag_awal 	= date($date_format);
		$tgl_flag_akhir = date($date_format, strtotime("+".$batas_interval." day"));
		$waktu_update	= date($date_format).' '.date('H:i:s');

		$status_persediaan = $this->set_status_persediaan($komponen_id, $golongan_id);
		
		$data_update = array(
							'status_persediaan'			=> $status_persediaan,
							'tgl_interval_awal' 		=> $tgl_flag_awal,
							'tgl_interval_akhir'		=> $tgl_flag_akhir,
							'waktu_update_persediaan'	=> $waktu_update
						);

		$kondisi = array(
						'komponen_id' => $komponen_id,
						'golongan_id' => $golongan_id
					);

		$update_status_persediaan = $this->db->where($kondisi)
											->update('jml_persediaan',$data_update);
		if ($update_status_persediaan) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	//
	public function update_status_persediaan($permintaan_kode)
	{
		//load "permintaan_model"
		$CI =& get_instance();
		$this->load->model('Permintaan_model', 'permintaan');

		$get_data_pengiriman = $this->permintaan->get_detail_pengiriman($permintaan_kode);
		$status 			 = '';

		foreach ($get_data_pengiriman as $key => $value) :

			$jumlah_pengiriman 	= $value->total_pengiriman;
			$golongan_id 		= $this->get_golongan_id($value->golongan_darah);
			$komponen_id 		= $this->get_komponen_id('WB');

			if ($jumlah_pengiriman > '0') {
				$update_interval = $this->update_interval_persediaan($komponen_id, $golongan_id);

				if (!$update_interval) {
					break;
					$status = 'error update status persediaan di WB - '.$value->golongan_darah;
				}
				else {
					$status = 'ok';
				}
			}
		
		endforeach;

		return $status;
	}
}

/* End of file Persediaan_model.php */
/* Location: ./application/models/Persediaan_model.php */