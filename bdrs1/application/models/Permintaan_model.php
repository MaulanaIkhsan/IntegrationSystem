<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Permintaan_model extends CI_Model {
	
	/*---------------- GENERATE PERMINTAAN ID ----------------*/
	public function get_counter()
	{
		$temp_counter = $this->db->select('counter_value')
						->from('counter')
						->where('counter_tabel', 'permintaan')
						->get()->result();

		return $temp_counter[0]->counter_value;
	}

	public function generate_id()
	{
		$temp_counter = $this->get_counter();

		switch (strlen($temp_counter)) {
			case 1: 
				$counter = '000'.$temp_counter; break;
			case 2: 
				$counter = '00'.$temp_counter; break;
			case 3: 
				$counter = '0'.$temp_counter; break;
			default: 
				$counter = $temp_counter; break;
		}

		return date('Ymd').date('His').$counter;
	}

	public function update_counter()
	{
		$data = array('counter_value' => $this->get_counter()+1);
		return $this->db->set($data)
						->where('counter_tabel', 'permintaan')
						->update('counter');
	}
	/*--------------------------------------------------------*/

	public function add_permintaan($kode_minta)
	{
		//$this->db->trans_begin();

		$generate_id = $this->generate_id();

		$data_minta = array(
					'permintaan_id' 		 => $generate_id,
					'petugas_id' 			 => $this->session->userdata('id'),
					'permintaan_kode' 		 => $kode_minta,
					'permintaan_tgl_pesan' 	 => date('Y-m-d'),
					'permintaan_jam_pesan'	 => date('H:i:s'),
					'permintaan_status' 	 => 'request',
					'permintaan_total' 		 => ($this->input->post('golongan_1')+$this->input->post('golongan_2')+$this->input->post('golongan_3')+$this->input->post('golongan_4')), 
					'pemasok_id'			 => $this->input->post('tujuan')
				);

		//input ke tabel permintaan
		
		$this->db->query('SET FOREIGN_KEY_CHECKS = 0'); //non-aktif foreign key cek

		if ($this->db->insert('permintaan', $data_minta)) {
			$minta = 'ok';

			//iterasi untuk input ke tabel detail permintaan
			$add = 1;
			for ($flag=1; $flag<=4 ; $flag++) { 
				$detail_minta = array(
						'detail_id' 	=> '',
						'permintaan_id' => $generate_id,
						'komponen_id' 	=> $this->input->post('komponen'),
						'golongan_id' 	=> $flag,
						'detail_jumlah' => $this->input->post('golongan_'.$flag) //ambil value dari textbox
					);
				if ($this->db->insert('detail_permintaan', $detail_minta)) {
					$add++;
				}
			}
			$this->update_counter();
			$this->db->query('SET FOREIGN_KEY_CHECKS = 1'); //non-aktif foreign key cek
			$minta .= $add;

			
		}
		else {
			$minta = 'cancel';
			//$this->db->trans_rollback();
		}
		
		return $minta;
	}

	public function show_permintaan()
	{
		return $this->db->select('permintaan_id as id_minta,permintaan_tgl_pesan as tgl_pesan, pemasok_nama as tujuan, petugas_nama as petugas, permintaan_total as total, permintaan_status as status')
						->from('permintaan')
						->join('pemasok', 'permintaan.pemasok_id = pemasok.pemasok_id')
						->join('petugas', 'permintaan.petugas_id = petugas.petugas_id')
						->where('permintaan_status', 'request')
						->or_where('permintaan_status', 'process')
						->get()->result();
	}

	public function detail($value)
	{
		return $this->db->select('permintaan_id as id_minta, permintaan_tgl_pesan as tgl_pesan, pemasok_nama as tujuan, petugas_nama as petugas, permintaan_total as total, permintaan_status as status, permintaan.permintaan_kode, pengiriman_permintaan.pengiriman_complete as is_complete')
						->from('permintaan')
						->join('pemasok', 'permintaan.pemasok_id = pemasok.pemasok_id')
						->join('petugas', 'permintaan.petugas_id = petugas.petugas_id')
						->join('pengiriman_permintaan', 'pengiriman_permintaan.permintaan_kode = permintaan.permintaan_kode')
						->where('permintaan_id', $value)
						->get()->row();
	}

	public function detail_komponen($value)
	{
		return $this->db->select('komponen_nama as komponen')
						->from('komponen_darah')
						->join('detail_permintaan', 'komponen_darah.komponen_id = detail_permintaan.komponen_id')
						->join('permintaan', 'detail_permintaan.permintaan_id = permintaan.permintaan_id')
						->where('permintaan.permintaan_id', $value)
						->group_by('komponen')
						->get()->row();

		/*------ QUERY SQL ------
		select 
			komponen_darah.komponen_nama as komponen
		from 
			komponen_darah 
		    	INNER JOIN detail_permintaan ON (komponen_darah.komponen_id = detail_permintaan.komponen_id)
		        INNER JOIN permintaan ON (detail_permintaan.permintaan_id = permintaan.permintaan_id)
		WHERE 
			permintaan.permintaan_id = '201606081815520001'
		GROUP BY 
			komponen*/
	}

	public function detail_golongan($value)
	{
		return $this->db->select('golongan_nama as golongan_darah, detail_jumlah as jumlah')
						->from('detail_permintaan')
						->join('golongan_darah', 'detail_permintaan.golongan_id = golongan_darah.golongan_id')
						->where('permintaan_id', $value)
						->get()->result();

		/*------ QUERY SQL ------
		select 
			golongan_darah.golongan_nama, detail_permintaan.detail_jumlah
		from 
			detail_permintaan
		    	INNER JOIN golongan_darah ON (detail_permintaan.golongan_id = golongan_darah.golongan_id)*/
	}

	public function show_edit($value)
	{
		return $this->db->select('detail_id, detail_jumlah, permintaan_id')
						->from('detail_permintaan')
						->where('permintaan_id', $value)
						->get()->result();
	}
	
	/*---------------------------------------------------------------------------------------------------*/
	// BAGIAN EDIT PERMINTAAN
	/*---------------------------------------------------------------------------------------------------*/
	public function get_permintaan_kode($permintaan_id)
	{
		return $this->db->select('permintaan_kode')
						->from('permintaan')
						->where('permintaan_id', $permintaan_id)
						->get()->row();
	}

	public function edit_permintaan($id)
	{
		//edit total permintaan pada tabel permintaan
		$total = $this->input->post('golongan_0') + $this->input->post('golongan_1') + $this->input->post('golongan_2') + $this->input->post('golongan_3');

		$data = array('permintaan_total' => $total);
		$edit_permintaan = $this->db->set($data)
					->where('permintaan_id', $id)
					->update('permintaan', $data);

		if ($edit_permintaan) {
			$edit = 'ok';

			//iterasi edit
			$flag = 0;
			for ($i=0; $i < 4; $i++) {
				$data = array('detail_jumlah' => $this->input->post('golongan_'.$i)); 
				$edit_detail = $this->db->where('detail_id', $this->input->post('id_'.$i))
							->update('detail_permintaan', $data);
				if ($edit_detail) {
					$flag++;
				}

			}
			$edit .= $flag;
			return $edit;
		}
		else {
			return 'cancel';
		}
	}

	/*---------------------------------------------------------------------------------------------------*/

	public function delete_permintaan($value)
	{
		if ( $this->db->delete('permintaan', $value)) {
			return 'ok';
		}
		else{
			return 'cancel';
		}
	}

	public function get_identitas()
	{
		$identitas = $this->db->select('identitas_nama')
					->from('identitas')
					->get()->row();
		return $identitas->identitas_nama;
	}

	public function ubah_status($pemesanan_id, $status)
	{
		$this->db->trans_begin();

		$data = array('permintaan_status' => $status);

		$update = $this->db->set($data)
						->where('permintaan_kode', $pemesanan_id)
						->update('permintaan', $data);

		if($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 'cancel';
		}
		else {
			$this->db->trans_commit();
			return 'ok';
		}
	}

	public function input_permintaan($kode_pemesanan, $data_darah)
	{
		if (is_array($data_darah)) {
			return 'hello world';
		}
		else {
			return 'data darah harus berbentuk array';
		}
	}

	public function show_sent_data()
	{
		return $this->db->select('permintaan_id as id_minta,permintaan_tgl_pesan as tgl_pesan, pemasok_nama as tujuan, petugas_nama as petugas, permintaan_total as total, permintaan_status as status, permintaan.permintaan_kode, pengiriman_permintaan.pengiriman_complete as is_complete')
						->from('permintaan')
						->join('pemasok', 'permintaan.pemasok_id = pemasok.pemasok_id')
						->join('petugas', 'permintaan.petugas_id = petugas.petugas_id')
						->join('pengiriman_permintaan', 'pengiriman_permintaan.permintaan_kode = permintaan.permintaan_kode')
						->where('permintaan_status', 'sent')
						->get()->result();
	}

	public function detail_sent_data($permintaan_id)
	{
		//mengambil permintaan_kode
		$kode 			 = $this->get_permintaan_kode($permintaan_id);
		$kode_permintaan = $kode->permintaan_kode;

		$query = "select 
						golongan.golongan_darah as golongan_darah,
						jml_single.total_single as jumlah_single,
						jml_double.total_double as jumlah_double,
						jumlah_terkirim.masuk_total as jumlah,
						(jml_single.total_single + (jml_double.total_double * 2)) as jumlah_total
					from
						(
							select
								darah_masuk.masuk_id as masuk_id_out, 
								golongan_darah.golongan_nama as golongan_darah
							from 
								darah_masuk
									inner join
								golongan_darah on(golongan_darah.golongan_id = darah_masuk.golongan_id)
							where
								darah_masuk.permintaan_kode = '$kode_permintaan'
							order by
								masuk_id
						) as golongan
							
							inner join
						
						(
							select
								darah_masuk.masuk_id as masuk_id_out, 
								masuk_perkantung.perkantung_total as total_single
							from
								masuk_perkantung
									inner join 
								darah_masuk on(darah_masuk.masuk_id = masuk_perkantung.masuk_id)
									inner join
								labu on(labu.labu_id = masuk_perkantung.labu_id)
							where
								labu.labu_jenis = 'Single'
									and
								darah_masuk.permintaan_kode = '$kode_permintaan'
							order by 
								perkantung_id
						) as jml_single
								
								on (golongan.masuk_id_out = jml_single.masuk_id_out)

							inner join
						(
							select
								darah_masuk.masuk_id as masuk_id_out, 
								masuk_perkantung.perkantung_total as total_double
							from
								masuk_perkantung
									inner join 
								darah_masuk on(darah_masuk.masuk_id = masuk_perkantung.masuk_id)
									inner join
								labu on(labu.labu_id = masuk_perkantung.labu_id)
							where
								labu.labu_jenis = 'Double'
									and
								darah_masuk.permintaan_kode = '$kode_permintaan'
							order by 
								perkantung_id
						) as jml_double

								on (golongan.masuk_id_out = jml_double.masuk_id_out)

							inner join
						(
							select
								masuk_id as masuk_id_out, 
								masuk_total 
							from 
								darah_masuk 
							where 
								permintaan_kode = '$kode_permintaan'
							order by 
								masuk_id
						) as jumlah_terkirim
								
								on(golongan.masuk_id_out = jumlah_terkirim.masuk_id_out)
						;";

		$run = $this->db->query($query);

		return $run->result();
	}

	// mengubah status permintaan darah pada tabel "permintaan" dan "pengiriman_permintaan" berdasarkan "permintaan_kode"
	public function konfirmasi_permintaan($permintaan_kode)
	{
		$data_update_permintaan = array(
						'permintaan_status'		 => 'done'
					);

		$data_update_pengiriman = array(
						'pengiriman_jam_konfirm' => date('H:i:s'),
						'pengiriman_tgl_konfirm' => date('Y-m-d'),
						'pengiriman_konfirm'	 => 'y'
					);

		$update_status_permintaan = $this->db->where('permintaan_kode', $permintaan_kode)
											->update('permintaan', $data_update_permintaan);
		
		$update_status_pengiriman = $this->db->where('permintaan_kode', $permintaan_kode)
											->update('pengiriman_permintaan', $data_update_pengiriman);
		
		if ($update_status_permintaan && $update_status_pengiriman) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	

	public function show_riwayat()
	{
		return $this->db->select('permintaan_id as id_minta,permintaan_tgl_pesan as tgl_pesan, pemasok_nama as tujuan, petugas_nama as petugas, permintaan_total as total, permintaan_status as status, permintaan.permintaan_kode as permintaan_kode, pengiriman_permintaan.pengiriman_complete as is_complete')
						->from('permintaan')
						->join('pemasok', 'permintaan.pemasok_id = pemasok.pemasok_id', 'inner')
						->join('petugas', 'permintaan.petugas_id = petugas.petugas_id', 'inner')
						->join('pengiriman_permintaan', 'pengiriman_permintaan.permintaan_kode = permintaan.permintaan_kode', 'inner')
						->where('permintaan.permintaan_status', 'done')
						->where('pengiriman_permintaan.pengiriman_konfirm', 'y')
						->get()->result();
	}

	public function detail_riwayat($permintaan_id)
	{
		return $this->db->select('permintaan_id as id_minta, permintaan_tgl_pesan as tgl_pesan, pemasok_nama as tujuan, petugas_nama as petugas, permintaan_total as total, permintaan_status as status, pengiriman_tgl_konfirm, pengiriman_jam_konfirm, permintaan_jam_pesan, pengiriman_permintaan.pengiriman_complete as is_complete')
						->from('permintaan')
						->join('pemasok', 'permintaan.pemasok_id = pemasok.pemasok_id', 'inner')
						->join('petugas', 'permintaan.petugas_id = petugas.petugas_id', 'inner')
						->join('pengiriman_permintaan', 'pengiriman_permintaan.permintaan_kode = permintaan.permintaan_kode', 'inner')
						->where('permintaan_id', $permintaan_id)
						->where('pengiriman_konfirm', 'y')
						->where('permintaan_status', 'done')
						->get()->row();
	}

	public function get_rincian_export_awal($permintaan_id)
	{
		return $this->db->select('permintaan_kode, pemasok_nama, permintaan_tgl_pesan, komponen_simbol, petugas_nama')
						->from('permintaan')
						->join('pemasok', 'permintaan.pemasok_id = pemasok.pemasok_id', 'inner')
						->join('detail_permintaan', 'permintaan.permintaan_id = detail_permintaan.permintaan_id', 'inner')
						->join('komponen_darah', 'detail_permintaan.komponen_id = komponen_darah.komponen_id', 'inner')
						->join('petugas', 'petugas.petugas_id = permintaan.petugas_id')
						->where('permintaan.permintaan_id', $permintaan_id)
						->get()->row();
	}

	//mengambil data darah untuk ditampilkan di formulir checklist penerimaan data darah
	public function get_darah_formulir($permintaan_id, $golongan_nama)
	{
		//mengambil permintaan_kode
		$kode 			 = $this->get_permintaan_kode($permintaan_id);
		$kode_permintaan = $kode->permintaan_kode;

		return $this->db->select('persediaan_barcode, persediaan_tgl_aftap, komponen_simbol, labu_jenis, golongan_nama, persediaan_rhesus, persediaan_tgl_kadaluwarsa')
						->from('persediaan')
						->join('komponen_darah', 'komponen_darah.komponen_id = persediaan.komponen_id', 'inner')
						->join('golongan_darah', 'golongan_darah.golongan_id = persediaan.golongan_id', 'inner')
						->join('labu', 'labu.labu_id = persediaan.labu_id', 'inner')
						->where('permintaan_kode', $kode_permintaan)
						->where('golongan_nama', $golongan_nama)
						->order_by('labu.labu_id, persediaan.persediaan_barcode', 'asc')
						->get()->result();
	}

	public function count_permintaan_keluar()
	{
		return $this->db->select('count(permintaan_id) as jumlah_keluar')
						->from('permintaan')
						->where('permintaan_status', 'request')
						->or_where('permintaan_status', 'process')
						->get()->row();
	}

	public function count_permintaan_masuk()
	{
		return $this->db->select('count(permintaan_id) as jumlah_masuk')
						->from('permintaan')
						->where('permintaan_status', 'sent')
						->get()->row();
	}

	//menambahakan data pengiriman permintaan dari UDD 
	public function add_pengiriman($permintaan_kode)
	{
		// $this->db->trans_begin();

		$data_pengiriman = array(
				'pengiriman_id'				=> '',
				'permintaan_kode'			=> $permintaan_kode,
				'pengiriman_complete' 		=> 'none',
				'pengiriman_konfirm'		=> 'n',
				'pengiriman_jam_konfirm'	=> '',
				'pengiriman_tgl_konfirm'	=> ''
			);

		$insert_pengiriman = $this->db->insert('pengiriman_permintaan', $data_pengiriman);

		if ($insert_pengiriman) {
			// $this->db->trans_commit();
			return 'ok';
		}
		else {
			// $this->db->trans_rollback();
			return 'cancel';
		}
	}


	public function update_pengiriman_complete($permintaan_kode, $is_complete)
	{
		$data_update = array('pengiriman_complete' => $is_complete);

		$update_pengiriman = $this->db->where('permintaan_kode', $permintaan_kode)
									->update('pengiriman_permintaan', $data_update);
		if ($update_pengiriman) {
			return 'ok';
		}
		else {
			return 'cancel';
		}

	}

	//mengambil "komponenen_id' dari tabel "komponen_darah" berdasarkan "komponen_nama"
	public function get_komponen_id($komponen_simbol)
	{
		$query = $this->db->select('komponen_id')
						->from('komponen_darah')
						->where('komponen_simbol', $komponen_simbol)
						->get()->row();

		return $query->komponen_id;
	}

	//mengambil "golongan_id' dari tabel "golongan_darah" berdasarkan "golongan_nama"
	public function get_golongan_id($golongan_nama)
	{
		$query = $this->db->select('golongan_id')
						->from('golongan_darah')
						->where('golongan_nama', $golongan_nama)
						->get()->row();

		return $query->golongan_id;
	}

	//mengambil "labu_id' dari tabel "labu" berdasarkan "labu_jenis"
	public function get_labu_id($labu_jenis)
	{
		$query = $this->db->select('labu_id')
						->from('labu')
						->where('labu_jenis', $labu_jenis)
						->get()->row();

		return $query->labu_id;
	}

	//mengambil "pengiriman_id" dari tabel "pengiriman_permintaan" berdasarkan "permintaan_kode"
	public function get_pengiriman_id($permintaan_kode)
	{
		return $this->db->select('pengiriman_id')
						->from('pengiriman_permintaan')
						->where('permintaan_kode', $permintaan_kode)
						->get()->row()
						->pengiriman_id;

	}

	//mengambil peng_gol_id dari tabel "pengiriman_golongan" berdasarkan "permintaan_kode, komponen_darah, golongan_darah"
	public function get_pengiriman_golongan_id($permintaan_kode, $komponen_darah, $golongan_darah)
	{
		$pengiriman_id 	= $this->get_pengiriman_id($permintaan_kode);
		$komponen_id 	= $this->get_komponen_id($komponen_darah);
		$golongan_id 	= $this->get_golongan_id($golongan_darah);

		$query = $this->db->select('peng_gol_id')
						->from('pengiriman_golongan')
						->where('pengiriman_id', $pengiriman_id)
						->where('komponen_id', $komponen_id)
						->where('golongan_id', $golongan_id)
						->get()->row();
		
		if (is_null($query)) {
			return 'kosong';
		}
		else {
			return $query->peng_gol_id;
		}
	}

	//mengecek apakah ada data pengiriman golongan pada tabel "pengiriman_golongan" berdasarkan 
	//"permintaan_kode, komponen_darah, golongan_darah"
	public function check_pengiriman_golongan($permintaan_kode, $komponen_darah, $golongan_darah)
	{
		//mengambil beberapa "id" terkait dari tabel lain
		$komponen_id 	= $this->get_komponen_id($komponen_darah);
		$golongan_id 	= $this->get_golongan_id($golongan_darah);
		$pengiriman_id 	= $this->get_pengiriman_id($permintaan_kode);

		$check = $this->db->select('count(peng_gol_id) as jumlah')
						->from('pengiriman_golongan')
						->where('pengiriman_id', $pengiriman_id)
						->where('komponen_id', $komponen_id)
						->where('golongan_id', $golongan_id)
						->get()->row();

		if ($check->jumlah == '0') {
			return 'kosong';
		}
		elseif ($check->jumlah > '0') {
			return 'ada';
		}
		else {
			return 'error';
		}
	}

	//mengecek apakah ada data pengiriman_kantung pada tabel "pengiriman_kantung" berdasarkan
	//"permintaan_kode, komponen_darah, golongan_darah, jenis_labu"
	public function check_pengiriman_kantung($permintaan_kode, $komponen_darah, $golongan_darah, $labu)
	{
		$pengiriman_golongan_id = $this->get_pengiriman_golongan_id($permintaan_kode, $komponen_darah, $golongan_darah);

		if ($pengiriman_golongan_id == 'kosong') {
			return 'kosong';
		}
		$check = $this->db->select('count(peng_kan_id) as jumlah')
						->from('pengiriman_kantung')
						->join('labu', 'pengiriman_kantung.labu_id = labu.labu_id', 'inner')
						->where('labu.labu_jenis', $labu)
						->where('peng_gol_id', $pengiriman_golongan_id)
						->get()->row();

		if ($check->jumlah == '0') {
			return 'kosong';
		}
		elseif ($check->jumlah > '0') {
			return 'ada';
		}
		else {
			return 'error';
		}
		//echo $pengiriman_golongan_id;
	}

	//mengambil "peng_gol_total" dari pengiriman_golongan 
	//berdasarkan "permintaan_kode, komponen_darah, golongan_darah"
	public function get_pengiriman_golongan_total($permintaan_kode, $komponen_darah, $golongan_darah)
	{
		$komponen_id 	= $this->get_komponen_id($komponen_darah);
		$golongan_id 	= $this->get_golongan_id($golongan_darah);
		$pengiriman_id 	= $this->get_pengiriman_id($permintaan_kode);
		
		return $this->db->select('peng_gol_total')
						->from('pengiriman_golongan')
						->where('pengiriman_id', $pengiriman_id)
						->where('komponen_id', $komponen_id)
						->where('golongan_id', $golongan_id)
						->get()
						->row()
						->peng_gol_total;

		// return 'pengiriman_id : '.$pengiriman_id.' - '.'komponen_id : '.$komponen_id.' - '.'golongan_id : '.$golongan_id;
	}

	//mengambil "peng_kan_total" dari pengiriman_golongan 
	//berdasarkan "permintaan_kode, komponen_darah, golongan_darah, jenis_labu"
	public function get_pengiriman_kantung_total($permintaan_kode, $komponen_darah, $golongan_darah, $labu)
	{
		$pengiriman_golongan_id = $this->get_pengiriman_golongan_id($permintaan_kode, $komponen_darah, $golongan_darah);

		return $this->db->select('peng_kan_total')
						->from('pengiriman_kantung')
						->join('labu', 'labu.labu_id = pengiriman_kantung.labu_id', 'inner')
						->where('labu.labu_jenis', $labu)
						->where('peng_gol_id', $pengiriman_golongan_id)
						->get()
						->row()
						->peng_kan_total;
	}

	//menambahkan pengiriman_golongan baru
	public function add_new_pengiriman_golongan($permintaan_kode, $komponen_darah, $golongan_darah, $jumlah)
	{
		//mengambil beberapa "id" terkait dari tabel lain
		$pengiriman_id 	= $this->get_pengiriman_id($permintaan_kode);
		$komponen_id 	= $this->get_komponen_id($komponen_darah);
		$golongan_id 	= $this->get_golongan_id($golongan_darah);

		$data_pengiriman_golongan = array(
									'peng_gol_id' 		=> '',
									'pengiriman_id'		=> $pengiriman_id,
									'komponen_id' 		=> $komponen_id,
									'golongan_id' 		=> $golongan_id,
									'peng_gol_total'	=> $jumlah
								);

		$insert_data = $this->db->insert('pengiriman_golongan', $data_pengiriman_golongan);

		if ($insert_data) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	//menambahkan pengiriman_kantung baru
	public function add_new_pengiriman_kantung($permintaan_kode, $komponen_darah, $golongan_darah, $labu, $jumlah)
	{
		//mengambil beberapa "id" terkait dari tabel lain
		$pengiriman_golongan_id = $this->get_pengiriman_golongan_id($permintaan_kode, $komponen_darah, $golongan_darah);
		$labu_id 				= $this->get_labu_id($labu);


		$data_pengiriman_kantung = array(
									'peng_kan_id'	 => '',
									'peng_gol_id'	 => $pengiriman_golongan_id,
									'labu_id'		 => $labu_id,
									'peng_kan_total' => $jumlah
								);

		$insert_data = $this->db->insert('pengiriman_kantung', $data_pengiriman_kantung);

		if ($insert_data) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	//mengupdate total pengiriman_golongan berdasarkan 
	//"permintaan_kode, komponen_darah, golongan_darah, jenis_labu"
	public function update_total_pengiriman_golongan($permintaan_kode, $komponen_darah, $golongan_darah, $jumlah)
	{
		//mengambil beberapa "id" terkait dari tabel lain
		$pengiriman_id 	= $this->get_pengiriman_golongan_id($permintaan_kode, $komponen_darah, $golongan_darah);
		$komponen_id 	= $this->get_komponen_id($komponen_darah);
		$golongan_id 	= $this->get_golongan_id($golongan_darah);
		
		$old_total = $this->get_pengiriman_golongan_total($permintaan_kode, $komponen_darah, $golongan_darah);
		$new_total = $jumlah + $old_total;

		$data_update = array('peng_gol_total' => $new_total);
		$kondisi	 = array(
						'pengiriman_id' => $pengiriman_id,
						'komponen_id'	=> $komponen_id,
						'golongan_id' 	=> $golongan_id
					);

		$run_update = $this->db->where($kondisi)
							->update('pengiriman_golongan', $data_update);
		
		if ($run_update) {
			return 'ok';
		}
		else {
			return 'cancel';
		}							
	}

	public function add_new_detail_pengiriman($detail_pengiriman)
	{
		$this->db->trans_begin();
		$error = '';

		$this->db->query('SET FOREIGN_KEY_CHECKS = 0'); //non-aktif foreign key cek

		$permintaan_kode 	= $detail_pengiriman['permintaan_kode'];
		$komponen_darah		= $detail_pengiriman['komponen_darah'];
		$golongan_darah		= $detail_pengiriman['golongan_darah'];
		$labu 				= $detail_pengiriman['jenis_kantung'];
		$jumlah 			= $detail_pengiriman['total'];

		$check_pengiriman_golongan 	= $this->check_pengiriman_golongan($permintaan_kode, $komponen_darah, $golongan_darah);
		$check_pengiriman_kantung	= $this->check_pengiriman_kantung($permintaan_kode, $komponen_darah, $golongan_darah, $labu);

		//proses checking, penambahan data, mengupdate data
		if (($check_pengiriman_golongan == 'kosong') && ($check_pengiriman_kantung == 'kosong')) {
			
			$add_pengiriman_golongan 	= $this->add_new_pengiriman_golongan($permintaan_kode, $komponen_darah, $golongan_darah, $jumlah);
			
			if ($add_pengiriman_golongan == 'ok') {
				
				$add_pengiriman_kantung 	= $this->add_new_pengiriman_kantung($permintaan_kode, $komponen_darah, $golongan_darah, $labu, $jumlah);
				
				if ($add_pengiriman_kantung) {
					$this->db->trans_commit();
					$this->db->query('SET FOREIGN_KEY_CHECKS = 1'); //aktifkan kembali foreign key cek
					return 'ok [kosong, kosong]';
				}
				else {
					$error = $error = 'gagal menambahkan pengiriman_kantung [baru, baru]';
					$this->db->trans_rollback();
					return $error;
				}//close else $add_pengiriman_kantung

			}//close if $add_pengiriman_golongan
			else {
				$error = 'gagal menambahkan pengiriman_golongan [baru, baru]';
				$this->db->trans_rollback();
				return $error;
			} //close else $add_pengiriman_golongan

		} //close if(baru, baru)
		elseif (($check_pengiriman_golongan == 'ada') && ($check_pengiriman_kantung == 'kosong')) {
			
			$update_pengiriman_golongan = $this->update_total_pengiriman_golongan($permintaan_kode, $komponen_darah, $golongan_darah, $jumlah);

			if ($update_pengiriman_golongan) {
				
				$add_pengiriman_kantung 	= $this->add_new_pengiriman_kantung($permintaan_kode, $komponen_darah, $golongan_darah, $labu, $jumlah);
				
				if ($add_pengiriman_kantung) {
					$this->db->query('SET FOREIGN_KEY_CHECKS = 1'); //aktifkan kembali foreign key cek
					$this->db->trans_commit();
					return 'ok [ada, kosong]';
				}
				else {
					$error = $error = 'gagal menambahkan pengiriman_kantung [baru, baru]';
					$this->db->trans_rollback();
					return $error;
				}//close else $add_pengiriman_kantung

			} //close if $update_pengiriman_golongan
			else {
				$error = 'gagal mengupdate pengiriman_golongan [ada, baru]';
				$this->db->trans_rollback();
				return $error;
			}//close else $update_pengiriman_golongan

		} //close elseif(ada, baru)
		else {
			$error = 'proses input detail pengiriman gagal dilakukan';
			$this->db->trans_rollback();
			return $error;
		}//close else

	} //close function add_new_detail_pengiriman

	//untuk mengambil pemasok_url dari tabel "pemasok" ketika proses edit dan membatalkan permintaan
	public function get_pemasok_url($permintaan_id)
	{
		return $this->db->select('pemasok_url')
						->from('pemasok')
						->join('permintaan', 'pemasok.pemasok_id = permintaan.pemasok_id', 'inner')
						->where('permintaan_id', $permintaan_id)
						->get()
						->row()
						->pemasok_url;
	}

	//untuk mengambil pemasok_url dari tabel pemasok ketika membuat permintaan baru
	public function get_pemasok_url_add($pemasok_id)
	{
		return $this->db->select('pemasok_url')
						->from('pemasok')
						->where('pemasok_id', $pemasok_id)
						->get()
						->row()
						->pemasok_url;
	}

	//mengambil jumlah data dari tabel "detail_permintaan" berdasarkan "permintaan_kode"
	public function get_detail_permintaan($permintaan_kode)
	{
		return $this->db->select('golongan_darah.golongan_nama as golongan_darah, detail_permintaan.detail_jumlah as jumlah_permintaan')
						->from('detail_permintaan')
						->join('permintaan', 'detail_permintaan.permintaan_id = permintaan.permintaan_id', 'inner')
						->join('golongan_darah', 'golongan_darah.golongan_id = detail_permintaan.golongan_id', 'inner')
						->where('permintaan.permintaan_kode', $permintaan_kode)
						->get()->result();
	}

	//mengambil data detail pengiriman berdasarkan "permintaan_kode"
	public function get_detail_pengiriman($permintaan_kode)
	{
		$query = "select
						data_single.golongan_darah as golongan_darah,
						data_single.total_single as jumlah_single,
						data_double.total_double as jumlah_double,
						(data_single.total_single + data_double.total_double) as total_pengiriman,
						(data_single.total_single + (data_double.total_double) * 2) as total_semua_pengiriman
					from
						(select 
							pengiriman_permintaan.permintaan_kode as permintaan_kode,
							pengiriman_golongan.peng_gol_id as pengiriman_golongan_id,
							golongan_darah.golongan_nama as golongan_darah,
							pengiriman_kantung.peng_kan_total as total_single
						from
							pengiriman_kantung
								inner join
							pengiriman_golongan on (pengiriman_golongan.peng_gol_id = pengiriman_kantung.peng_gol_id)
								inner join
							pengiriman_permintaan on (pengiriman_permintaan.pengiriman_id = pengiriman_golongan.pengiriman_id)
								inner join
							labu on (labu.labu_id = pengiriman_kantung.labu_id)
								inner join
							golongan_darah on (pengiriman_golongan.golongan_id = golongan_darah.golongan_id)
						where
							pengiriman_permintaan.permintaan_kode = '$permintaan_kode'
								and
							labu.labu_jenis = 'Single'
						order by
							pengiriman_kantung.peng_kan_id) as data_single

							inner join

						(select 
							pengiriman_golongan.peng_gol_id as pengiriman_golongan_id,
							pengiriman_kantung.peng_kan_total as total_double
						from
							pengiriman_kantung
								inner join
							pengiriman_golongan on (pengiriman_golongan.peng_gol_id = pengiriman_kantung.peng_gol_id)
								inner join
							pengiriman_permintaan on (pengiriman_permintaan.pengiriman_id = pengiriman_golongan.pengiriman_id)
								inner join
							labu on (labu.labu_id = pengiriman_kantung.labu_id)
								inner join
							golongan_darah on (pengiriman_golongan.golongan_id = golongan_darah.golongan_id)
						where
							pengiriman_permintaan.permintaan_kode = '$permintaan_kode'
								and
							labu.labu_jenis = 'Double'
						order by
							pengiriman_kantung.peng_kan_id) as data_double
							on (data_single.pengiriman_golongan_id = data_double.pengiriman_golongan_id)
						;";

		$run = $this->db->query($query);

		return $run->result();
	}

	
}

/* End of file Permintaan_model.php */
/* Location: ./application/models/Permintaan_model.php */