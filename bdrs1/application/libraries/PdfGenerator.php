<?php
/**
* 
*/
class PdfGenerator
{
	public function generate($html, $filename)
	{
		define('DOMPDF_ENABLE_AUTOLOAD', false);
		define("DOMPDF_ENABLE_REMOTE", true);
		define("DOMPDF_ENABLE_CSS_FLOAT", true);
	    require_once("./vendor/dompdf/dompdf/dompdf_config.inc.php");
	 
	    $dompdf = new DOMPDF();
	    $dompdf->load_html($html);
	    $dompdf->render();
	    $dompdf->set_paper("A4", "portrait");
	    $dompdf->stream($filename.'.pdf',array("Attachment"=>0));
	    $dompdf->set_base_path(base_url().'assets/'); //men-load plugin tambahan(bisa css atau js file)
	}
}