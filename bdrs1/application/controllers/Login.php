<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() === FALSE) {
			$this->load->view('login/form_login');
		} 
		else {
			$this->load->model('Login_model', 'login');
			$data_user = $this->login->check_login();

			if ($data_user === FALSE) {
				$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa  fa-hand-paper-o"></i> Kombinasi username dan password salah ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');

				redirect('login');
			}
			else {
				$this->session->set_userdata('id', $data_user->petugas_id);
				$this->session->set_userdata('nama', $data_user->petugas_nama);
				$this->session->set_userdata('username', $data_user->petugas_username);

				$this->session->set_flashdata('success','<div class="alert alert-success text-center"><i class="fa fa-info-circle"></i> Selamat datang '.$data_user->petugas_nama.'! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');

				redirect('beranda');
			}
		}

	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */

/*refrensi form : http://www.bootply.com/enqg8DKjHG*/