<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if ($this->session->userdata('username') === NULL) {
			$this->session->set_flashdata('warning','<div class="alert alert-warning text-center"><i class="fa  fa-warning "></i> Harap login terlebih dahulu ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
			redirect('login');
		}
	}

	public function index()
	{
		$this->load->model('Persediaan_model', 'persediaan');
		$this->load->model('Permintaan_model', 'permintaan');

		$data['data_persediaan'] 	= $this->persediaan->get_jml_persediaan();
		$data['permintaan_keluar'] 	= $this->permintaan->count_permintaan_keluar();
		$data['permintaan_masuk']	= $this->permintaan->count_permintaan_masuk();
		
		$this->load->view('beranda/after_login', $data);		
	}

}

/* End of file Beranda.php */
/* Location: ./application/controllers/Beranda.php */