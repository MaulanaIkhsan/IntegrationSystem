<?php $this->load->view('template/header'); ?>

<!-- Tempat meletakkan plugin pada bagian header -->

<?php $this->load->view('template/top-nav'); ?>
<?php $this->load->view('template/side-nav'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="col-md-12"><?php echo $this->session->flashdata('success'); ?></div>
      <div class="col-md-12"><?php echo $this->session->flashdata('error'); ?></div>
      
      <!-- Main row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-files-o"></i> <strong>Jumlah Persediaan Darah</strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-12">&nbsp;</div>
              <div class="col-md-12">
              
              <h4>Komponen Darah : Whole Blood (WB)</h4>
              <div>&nbsp;</div>
                <table>
                  <thead>
                    <tr>
                      <th rowspan="2" valign="middle"><center>Golongan Darah</center></th>
                      <th rowspan="2" valign="middle"><center>Status</center></th>
                      <th colspan="2"><center>Jenis Kantung</center></th>
                      <th rowspan="2"><center>Jumlah Total</center></th>
                    </tr>
                    <tr>
                      <th><center>Single</center></th>
                      <th><center>Double</center></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <?php foreach ($jml_persediaan as $key) : ?>
                        <tr>
                          <td class="text-center"><?php echo $key->golongan_darah; ?></td>
                          <td class="text-center">
                            <?php if ($key->status_persediaan == 'Gawat') { ?>
                              <small class="label bg-red"><?php echo $key->status_persediaan; ?></small>
                            <?php } elseif ($key->status_persediaan == 'Waspada') { ?>
                              <small class="label bg-yellow"><?php echo $key->status_persediaan; ?></small>
                            <?php } 
                               else { ?>
                              <small class="label bg-green"><?php echo $key->status_persediaan; ?></small>
                            <?php } ?>   
                          </td>
                          <td class="text-center"><a href="<?php echo site_url('persediaan/detail/'.$key->golongan_darah.'/Single'); ?>"><?php echo $key->total_single; ?></a></td>
                          <td class="text-center"><a href="<?php echo site_url('persediaan/detail/'.$key->golongan_darah.'/Double'); ?>"><?php echo $key->total_double; ?></a></td>
                          <td class="text-center"><?php echo $key->total_semuanya; ?></td>
                        </tr>
                      <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div> <!-- close box-body -->
          </div> <!-- close box-success -->
        </div> <!-- close col-md-12 -->
      </div> <!-- close row -->
    </section>
    <!-- /.content -->
  </div>
  
  <?php $this->load->view('template/info-footer.php'); ?>

</div>
<!-- ./wrapper -->

<!-- Tempat meletakkan plugin pada bagian footer -->

<?php $this->load->view('template/footer'); ?>
