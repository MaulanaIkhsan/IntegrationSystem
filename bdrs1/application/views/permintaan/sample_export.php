<!DOCTYPE html>
<html>
<head>
	<title>Sample Export</title>
	<style type="text/css">

		#layout {
			float: none;
			width: 800px;
			/*border: 1px solid black;*/
			margin-right: 30px;
		}

		#top {
			float: none;
			width: 300px;
			height: 100px;
			background-color: red;
		}

		#left {
			float: left;
			width: 200px;
			height: 300px;
			background-color: blue;
			color: white;
		}

		#main {
			float: right;
			width: 100px;
			height: 300px;
			background-color: #999999;
			margin-right: 200px;
		}
	</style>
</head>
<body>
	<div id="layout">
		<div id="top">Header</div>
		<div id="left">Left Content</div>
		<div id="main">Main Content</div>
	</div>
</body>
</html>