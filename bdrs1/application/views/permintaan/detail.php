<?php $this->load->view('template/header'); ?>

<!-- Tempat meletakkan plugin pada bagian header -->

<?php $this->load->view('template/top-nav'); ?>
<?php $this->load->view('template/side-nav'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="col-md-12"><?php echo $this->session->flashdata('success'); ?></div>
      <div class="col-md-12"><?php echo $this->session->flashdata('error'); ?></div>
      
      <!-- Main row -->
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-question"></i> <strong>Detail Permintaan Darah</strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-5">
              	<div class="pull-right">Tanggal Pesan</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
              	<div class="pull-left"><?php echo date("d-M-Y", strtotime($detail->tgl_pesan)); ?></div>
              </div> 
              <div class="col-md-5">
              	<div class="pull-right">Petugas</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
              	<div class="pull-left"><?php echo $detail->petugas; ?></div>
              </div> 
              <div class="col-md-5">
              	<div class="pull-right">Tujuan</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
              	<div class="pull-left"><?php echo $detail->tujuan; ?></div>
              </div>
              <div class="col-md-5">
              	<div class="pull-right">Status</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
              	<div class="pull-left"><?php echo $detail->status; ?></div>
              </div>
              <div class="col-md-5">
                <div class="pull-right">Pengiriman</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
                <div class="pull-left">
                  <?php if ($detail->total < 50) { ?>
                    <small class="label bg-yellow">Mandiri</small>
                  <?php } else { ?>
                    <small class="label bg-green">Antar</small>
                  <?php } ?>
                </div>
              </div>
              <div class="col-md-12">&nbsp;</div>
              <div class="col-md-5">
              	<div class="pull-right">Komponen : <?php echo $detail_komponen->komponen; ?></div>
              </div>
              <div class="col-md-12">&nbsp;</div>
              <div class="col-md-8 col-md-offset-2">
              	<table>
                <thead>
                  <tr>
                    <th><center>Golongan Darah</center></th>
                    <th><center>Jumlah</center></th>
                  </tr>
                </thead>
                <tbody>
              		<?php foreach ($detail_golongan as $value) { ?>
              			<tr>
              				<td><center><?php echo $value->golongan_darah; ?></center></td>
              				<td><div class="pull-right"><?php echo $value->jumlah; ?></div></center></td>
              			</tr>
              		<?php } ?>
                		<tr>
                			<td><center>Total</center></td>
                			<td><div class="pull-right"><?php echo $detail->total; ?></div></td>
                		</tr>
                  </tbody>
              	</table>
              </div> 
            </div> <!-- close box-body -->
            <div class="box-footer">
            	<a href="<?php echo site_url('permintaan/show_minta'); ?>" class="btn btn-danger pull-left"><i class="fa fa-backward"></i> Kembali</a>
              
              <?php if($detail->status === 'request') {?>
                <div class="pull-right">
                <a href="<?php echo site_url('permintaan/edit/'.$detail->id_minta)?>" class="btn btn-primary"><i class="fa fa-pencil"></i> Ubah</a>
                <a href="#" data-toggle="modal" data-target="#modal_konfirm" class="btn btn-danger"><i class="fa fa-close"></i> Batalkan</a>
              
                        <!-- konfirm dialog -->

                          <div class="modal fade" id="modal_konfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> <strong>Konfirmasi Pembatalan Permintaan</strong></h4>
                                </div>
                                <div class="modal-body">
                                  Apakah anda ingin membatalkan pemesanan tgl "<?php echo date("d-M-Y", strtotime($detail->tgl_pesan)); ?>" ke  "<?php echo $detail->tujuan; ?>" ?
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Tidak</button>
                                  <!-- <button type="button" class="btn btn-primary"></button> -->
                                  <a href="<?php echo site_url('permintaan/delete/'.$detail->id_minta)?>" class="btn btn-primary"><i class="fa fa-check"></i> Ya, batalkan</a>
                                </div>
                              </div>
                            </div>
                          </div>
              <?php }?>
            	

              </div>
            </div>
          </div> <!-- close box-success -->
        </div> <!-- close col-md-12 -->
      </div> <!-- close row -->
    </section>
    <!-- /.content -->
  </div>
  
  <?php $this->load->view('template/info-footer.php'); ?>

</div>
<!-- ./wrapper -->

<!-- Tempat meletakkan plugin pada bagian footer -->

<?php $this->load->view('template/footer'); ?>
