<?php $this->load->view('template/header'); ?>

<!-- Tempat meletakkan plugin pada bagian header -->

<?php $this->load->view('template/top-nav'); ?>
<?php $this->load->view('template/side-nav'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    
    <!-- Main content -->
    <section class="content">
      
      <!-- Main row -->
      <div class="row">
      
      <div class="col-md-12"><?php echo $this->session->flashdata('error'); ?></div>

        <div class="col-md-10 col-md-offset-1">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-plus"></i> <strong>Tambah Permintaan</strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <form class="form-horizontal col-md-offset-2" action="<?php echo site_url('permintaan/add'); ?>" method="post">
              <div class="form-group">
                 <label for="inputTujuan" class="control-label col-md-2">Tujuan :</label>
                 <div class="col-md-5">
                   <select name="tujuan" class="form-control" required="true">
                     <option value="">--Pilih--</option>
                     <?php foreach ($pemasok as $value) { ?>
                       <option value="<?php echo $value->pemasok_id; ?>" selected="true"> <?php echo $value->pemasok_nama; ?> </option>
                     <?php } ?>
                   </select>
                  <span class="text-danger col-md-8"><?php echo form_error('tujuan'); ?></span>
                 </div>
               </div>
               <div class="form-group">
                 <label for="inputKomponen" class="control-label col-md-2">Komponen :</label>
                 <div class="col-md-5">
                   <select name="komponen" class="form-control" required="true">
                     <option value="">--Pilih--</option>
                     <?php foreach ($komponen as $value) { ?>
                       <option value="<?php echo $value->komponen_id; ?>" selected="true"> <?php echo $value->komponen_nama; ?> </option>
                     <?php } ?>
                   </select>
                   <span class="text-danger col-md-8"><?php echo form_error('komponen'); ?></span>
                 </div>
               </div>

               <div class="form-group">
                 <label for="inputA" class="control-label col-md-2">A :</label>
                 <div class="col-md-5">
                   <input type="number" name="golongan_1" class="form-control" required="true" />
                   <span class="text-danger col-md-8"><?php echo form_error('golongan_1'); ?></span>
                 </div>
               </div>
               <div class="form-group">
                 <label for="inputB" class="control-label col-md-2">B :</label>
                 <div class="col-md-5">
                   <input type="number" name="golongan_2" class="form-control" required="true" />
                   <span class="text-danger col-md-8"><?php echo form_error('golongan_2'); ?></span>
                 </div>
               </div>
               <div class="form-group">
                 <label for="inputO" class="control-label col-md-2">O :</label>
                 <div class="col-md-5">
                   <input type="number" name="golongan_3" class="form-control" required="true" />
                   <span class="text-danger col-md-8"><?php echo form_error('golongan_3'); ?></span>
                 </div>
               </div>
               <div class="form-group">
                 <label for="inputAB" class="control-label col-md-2">AB :</label>
                 <div class="col-md-5">
                   <input type="number" name="golongan_4" class="form-control" required="true" />
                   <span class="text-danger col-md-8"><?php echo form_error('golongan_4'); ?></span>
                 </div>
               </div>
            </div> <!-- close box-body -->
            <div class="box-footer">
              <a href="<?php echo site_url('permintaan/show_minta'); ?>" class="btn btn-danger pull-left"><i class="fa fa-backward"></i> Kembali</a>
              <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Tambah Permintaan</button>
            </div> <!-- close box-footer -->
          </div> <!-- close box -->
         </form>
        </div> <!-- close col-md-12 -->
      </div> <!-- close row -->
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  
  <?php $this->load->view('template/info-footer.php'); ?>

</div>
<!-- ./wrapper -->

<!-- Tempat meletakkan plugin pada bagian footer -->

<?php $this->load->view('template/footer'); ?>