<?php $this->load->view('template/header'); ?>

<!-- Tempat meletakkan plugin pada bagian header -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/css/dataTables.bootstrap.min.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/css/jquery.dataTables.min.css'); ?>" />

<script type="text/javascript" src="<?php echo base_url('assets/datatable/js/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/js/dataTables.bootstrap.min.js') ?>"></script>

<?php $this->load->view('template/top-nav'); ?>
<?php $this->load->view('template/side-nav'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="col-md-12"><?php echo $this->session->flashdata('success'); ?></div>
      <div class="col-md-12"><?php echo $this->session->flashdata('error'); ?></div>
      
      <!-- Main row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-file-o"></i> <strong>Data Permintaan Darah</strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-12 pull-left">
                <a href="<?php echo site_url('permintaan/add'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> Minta Baru</a>
              </div>
              <div class="col-md-12">&nbsp;</div>
              <div class="col-md-12">
                <table id="tabelku">
                  <thead>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Tanggal Pesan</center></th>
                      <th><center>Tujuan</center></th>
                      <th><center>Petugas</center></th>
                      <th><center>Total</center></th>
                      <th><center>Status</center></th>
                      <th><center>Pengiriman</center></th>
                      <th><center>Operasi</center></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $no = 1;
                    foreach($permintaan as $value) :
                    ?>
                      <?php if ($value->status === 'process') {?>
                        <tr class="success-self">
                      <?php } else {?>
                        <tr>
                      <?php } ?>
                      <td><center><?php echo $no; ?></center></td>
                      <td><center><?php echo date("d-M-Y", strtotime($value->tgl_pesan)); ?></center></td>
                      <td><?php echo $value->tujuan; ?></td>
                      <td><?php echo $value->petugas; ?></td>
                      <td><center><?php echo $value->total; ?></center></td>
                      <td><center><?php echo $value->status; ?></center></td>
                      <td><center>
                        <?php if ($value->total < 50) { ?>
                          <small class="label bg-yellow">Mandiri</small>
                        <?php } else { ?>
                          <small class="label bg-green">Antar</small>
                        <?php } ?>
                      </center></td>
                      
                      <?php if ($value->status === 'request') {?>
                        <td>
                          <center>
                            <a href=<?php echo site_url('permintaan/detail/'.$value->id_minta);?> class="btn btn-primary"><i class="fa fa-question"></i> Detail</a> | 
                            <a href=# data-toggle="modal" data-target="<?php echo '#modal_'.$no; ?>" class="btn btn-danger"><i class="fa fa-close"></i> Batalkan</a>
                          </center>
                        </td>
                      <?php } else {?>
                        <td colspan="2">
                          <center><a href=<?php echo site_url('permintaan/detail/'.$value->id_minta);?> class="btn btn-primary"><i class="fa fa-question"></i> Detail</a></center>
                        </td>
                      <?php }?>
                    </tr>

                    <!-- konfirm dialog -->

                    <div class="modal fade" id="<?php echo 'modal_'.$no; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> <strong>Konfirmasi Pembatalan Permintaan</strong></h4>
                          </div>
                          <div class="modal-body">
                            Apakah anda ingin membatalkan pemesanan tgl "<?php echo date("d-M-Y", strtotime($value->tgl_pesan)); ?>" ke  "<?php echo $value->tujuan; ?>" ?
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Tidak</button>
                            <a href="<?php echo site_url('permintaan/delete/'.$value->id_minta)?>" class="btn btn-primary"><i class="fa fa-check"></i> Ya, batalkan</a>
                          </div>
                        </div>
                      </div>
                    </div>

                    <?php 
                    $no++;
                    endforeach;
                    ?>
                  </tbody>
                </table>
              </div>
            </div> <!-- close box-body -->
          </div> <!-- close box-success -->
        </div> <!-- close col-md-12 -->
      </div> <!-- close row -->
    </section>
    <!-- /.content -->
  </div>
  
  <?php $this->load->view('template/info-footer.php'); ?>

</div>
<!-- ./wrapper -->

<!-- Tempat meletakkan plugin pada bagian footer -->
<script type="text/javascript">
  $(document).ready(function(){
    var data_persediaan = $("#tabelku").DataTable({
                            "pageLength": 25
                          });

    setInterval(function test(){
      data_persediaan.fnDraw();
    }, 1000);
  });
</script>

<?php $this->load->view('template/footer'); ?>
