<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
     &nbsp;
    </div>
   
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header" id="nav-header">DAFTAR MENU</li>
      <li>
        <a href="<?php echo site_url('awal'); ?>">
          <i class="fa fa-home"></i><span>Beranda</span></i>
        </a>
      </li>
      <li>
        <a href="<?php echo site_url('login'); ?>">
          <i class="fa fa-user"></i>
          <span>Masuk</span>
        </a>
      </li>
     
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>