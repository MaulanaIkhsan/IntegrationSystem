<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url('assets/self/img/user.png'); ?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info" id="own-info">
        <p><?php echo $this->session->userdata('nama'); ?></p>
        
      </div>
    </div>
   
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header" id="nav-header">DAFTAR MENU</li>
      <li>
        <a href="<?php echo site_url('beranda'); ?>">
          <i class="fa fa-home"></i><span>Beranda</span></i>
        </a>
      </li>
      <li>
        <a href="<?php echo site_url('persediaan'); ?>">
          <i class="fa fa-files-o"></i>
          <span>Persediaan</span>
        </a>
      </li>
      <li>
        <a href="pages/widgets.html">
          <i class="fa fa-paste"></i> <span>Permintaan</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url('permintaan/show_minta'); ?>"><i class="fa fa-list-alt"></i>Minta</a></li>
          <li><a href="<?php echo site_url('permintaan/show_all_sent'); ?>"><i class="fa fa-list-alt"></i>Terima</a></li>
           <li><a href="<?php echo site_url('permintaan/riwayat'); ?>"><i class="fa fa-history"></i>Riwayat</a></li>
        </ul>
      </li>
     
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>