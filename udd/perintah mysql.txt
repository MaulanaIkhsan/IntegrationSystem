--- Menampilkan Batas Minimal dan Batas Aman Pada Setiap Komponen dan Golongan Darah ---
SELECT 
	bdrs1_komp_darah.BDRS1_komp_simbol as komponen,
    bdrs1_gol_darah.BDRS1_gol_nama as golongan_darah,
    bdrs1_batas_persediaan.BDRS1_btper_minimal as batas_minimal,
    bdrs1_batas_persediaan.BDRS1_btper_aman as batas_aman
FROM
	bdrs1_batas_persediaan 
    INNER JOIN bdrs1_komp_darah 
    	ON bdrs1_batas_persediaan.BDRS1_btper_komp_id = bdrs1_komp_darah.BDRS1_komp_id
    INNER JOIN bdrs1_gol_darah
    	ON bdrs1_batas_persediaan.BDRS1_btper_gol_id = bdrs1_gol_darah.BDRS1_gol_id;

--- Mengubah Format Tanggal pada Tipe Data DATE ---
select date_format('1995-08-12', '%d %b %Y');

--- Menghitung Hari dari Tanggal Kadaluwarsa ---
select concat(datediff(curdate(), '2016-05-28'), ' ', 'hari') as umur;