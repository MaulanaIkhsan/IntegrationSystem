<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persediaan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		if (($this->session->userdata('username_udd') == NULL) && ($this->session->userdata('nama_udd') == NULL) && ($this->session->userdata('id_udd') == NULL)) {
			$this->session->set_flashdata('warning','<div class="alert alert-warning text-center"><i class="fa  fa-warning "></i> Harap login terlebih dahulu ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
			redirect('login');
		}
	}

	// List all your items
	public function index()
	{
		$this->load->model('Persediaan_model', 'persediaan');
		
		$data['persediaan'] = $this->persediaan->get_persediaan();
		$data['category'] 	= $this->persediaan->get_persediaan_komponen();

		$this->load->view('persediaan/show', $data);
	}

	public function detail($type_blood, $pocket_name)
	{
		$this->load->model('Persediaan_model', 'persediaan');
		
		$data['golongan_darah'] 	= $type_blood;
		$data['jenis_kantung']		= $pocket_name;
		$data['komponen']			= 'Whole Blood (WB)';
		$data['detail_persediaan'] 	= $this->persediaan->get_detail_persediaan($type_blood, $pocket_name);

		$this->load->view('persediaan/detail', $data);
		//echo 'Golongan Darah : '.$type_blood.' Jenis Kantung : '.$pocket_name;
	}

	public function checking_stock()
	{
		$this->load->model('Persediaan_model', 'persediaan');

		$jml_A_single 	= $this->persediaan->get_count_type_blood('WB', 'A', 'Single');
		$jml_A_double 	= $this->persediaan->get_count_type_blood('WB', 'A', 'Double');
		$jml_B_single 	= $this->persediaan->get_count_type_blood('WB', 'B', 'Single');
		$jml_B_double 	= $this->persediaan->get_count_type_blood('WB', 'B', 'Double');
		$jml_O_single 	= $this->persediaan->get_count_type_blood('WB', 'O', 'Single');
		$jml_O_double 	= $this->persediaan->get_count_type_blood('WB', 'O', 'Double');
		$jml_AB_single 	= $this->persediaan->get_count_type_blood('WB', 'AB', 'Single');
		$jml_AB_double 	= $this->persediaan->get_count_type_blood('WB', 'AB', 'Double');

		$total_A 	= $jml_A_single + $jml_A_double;
		$total_B 	= $jml_B_single + $jml_B_double;
		$total_O 	= $jml_O_single + $jml_O_double;
		$total_AB 	= $jml_AB_single + $jml_AB_double;

		echo 'A single : '.$jml_A_single.'<br/>';
		echo 'A double : '.$jml_A_double.'<br/>';
		echo 'B single : '.$jml_B_single.'<br/>';
		echo 'B double : '.$jml_B_double.'<br/>';
		echo 'O single : '.$jml_O_single.'<br/>';
		echo 'O double : '.$jml_O_double.'<br/>';
		echo 'AB single : '.$jml_AB_single.'<br/>';
		echo 'AB double : '.$jml_AB_double.'<br/>';
	}

	public function dummy_detail_stock()
	{
		$this->load->model('Persediaan_model', 'persediaan');
		$this->persediaan->add_dummy_stock();
		
		echo 'dummy data berhasil dibuat';
	}

	public function sample()
	{
		$this->load->model('Persediaan_model', 'persediaan');

		$data['data_persediaan'] = $this->persediaan->get_data_persediaan();
		$this->load->view('persediaan/sample_datatable', $data);

	}

	public function data_json()
	{
		$this->load->model('Persediaan_model', 'persediaan');

		echo json_encode($this->persediaan->get_data_persediaan());
	}
}

/* End of file Persediaan.php */
/* Location: ./application/controllers/Persediaan.php */
