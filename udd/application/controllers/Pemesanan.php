<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//belum bisa mencatat log darah keluar, tetapi sudah bisa mengirim data

class Pemesanan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if (($this->session->userdata('username_udd') == NULL) && ($this->session->userdata('nama_udd') == NULL) && ($this->session->userdata('id_udd') == NULL)) {
			$this->session->set_flashdata('warning','<div class="alert alert-warning text-center"><i class="fa  fa-warning "></i> Harap login terlebih dahulu ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
			redirect('login');
		}

		$this->load->library('nusoap_library');
	}
	public function index()
	{
		$this->load->model('Pemesanan_model', 'pemesanan');

		$data['data_pemesanan'] = $this->pemesanan->show_pemesanan();
		$this->load->view('pemesanan/show', $data);
	}

	public function detail($value)
	{
		$this->load->model('Pemesanan_model', 'pemesanan');

		$data_pemesanan 		= $this->pemesanan->detail_request($value);
		$detail_data_pemesanan 	= $this->pemesanan->detail_type($value);

		if (empty($data_pemesanan) && empty($detail_data_pemesanan)) {
			$this->session->set_flashdata('error','<div class="alert alert-warning text-center"><i class="fa  fa-warning "></i> <strong>Detail Pemesanan Darah Yang Anda Cari Tidak Tersedia</strong> <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
			redirect('pemesanan');
		}
		else {
			$data['detail_pemesanan'] 	= $this->pemesanan->detail_request($value);
			$data['detail_category']	= $this->pemesanan->detail_category($value);
			//detail data pemesanan darah
			$data['detail_type']		= $this->pemesanan->detail_type($value); 

			$this->load->view('pemesanan/detail', $data);
		}

		
	}

	/*
	Untuk mengeksekusi/memproses lebih lanjut permintaan dari BDRS. 
	Setelah dijalakan maka status permintaan juga akan dirubah. 
	status request yang bisa dirubah yaitu "process & sent" 
	*/
	public function execute_request($request_id)
	{
		$this->load->model('Pemesanan_model', 'pemesanan');
		
		//ambil URL dari berdasarkan request_id
		$url = $this->pemesanan->get_url($request_id);

		$client = new nusoap_client($url->consumer_url);

		$data = array('kode_pesan' => $request_id, 'status' => 'process');

		$result = $client->call('ubah_status', $data);

		if ($client->fault) {
			echo 'fault : '.$result;
		}
		else {
			$error = $client->getError();
			if ($error) {
				echo 'error : '.$error;
			}
			else {
				if ($result === 'ok') {
					$edit_local = $this->pemesanan->change_status($request_id, 'process');
					if ($edit_local) {
						$this->session->set_flashdata('success','<div class="alert alert-info text-center"><i class="fa fa-info-circle"></i> Status pemesanan berhasil diubah<a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');

						redirect('pemesanan/kirim_data/'.$request_id);
					}
					else{
						$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Status pemesanan gagal tersimpan ! (awal) <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');

						redirect('pemesanan');
					}

				}
				else {
					$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Status pemesanan gagal tersimpan ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
						
					redirect('pemesanan');
				}
			}
		}
		
		/*alter sequence receipt_pocket_recpocket_id_seq restart with 1;
		alter sequence receipt_request_receipt_id_seq restart with 1;*/
	}

	public function kirim_data($request_id)
	{
		$this->load->model('Pemesanan_model', 'pemesanan');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('jml_single_1', 'golongan A tipe single', 'integer|required');
		$this->form_validation->set_rules('jml_double_1', 'golongan A tipe double', 'integer|required');
		$this->form_validation->set_rules('jml_single_2', 'golongan B tipe single', 'integer|required');
		$this->form_validation->set_rules('jml_double_2', 'golongan B tipe double', 'integer|required');
		$this->form_validation->set_rules('jml_single_3', 'golongan O tipe single', 'integer|required');
		$this->form_validation->set_rules('jml_double_3', 'golongan O tipe double', 'integer|required');
		$this->form_validation->set_rules('jml_single_4', 'golongan AB tipe single', 'integer|required');
		$this->form_validation->set_rules('jml_double_4', 'golongan AB tipe double', 'integer|required');		

		if ($this->form_validation->run() == FALSE) {
			$data['detail_item'] = $this->pemesanan->get_request_data($request_id);
			$data['request']	 = $request_id;
			$this->load->view('pemesanan/send', $data);
		}
		else {

			ini_set("soap.wsdl_cache_enabled", 0);

			$this->db->trans_begin();

			$is_complete = '';
			//$update_stock_blood = $this->pemesanan->update_stock();
			$total_request 		= $this->pemesanan->get_total_request($request_id);
			$total_semua_input	= $this->input->post('jumlah_semua');

			if ($total_semua_input < $total_request) {
				$is_complete = 'incomplete';
			}
			
			if($total_semua_input == $total_request){
				$is_complete = 'complete';
			}

			if($total_semua_input > $total_request) {
				$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Total pengiriman melebihi jumlah yang diminta, harap ulangi kembali  ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');

				redirect('pemesanan/kirim_data/'.$request_id);
			}

			//mengambil data dari form
			$jml_A_single 	= $this->input->post('jml_single_1');
			$jml_A_double 	= $this->input->post('jml_double_1');
			$jml_B_single 	= $this->input->post('jml_single_2');
			$jml_B_double 	= $this->input->post('jml_double_2');
			$jml_O_single 	= $this->input->post('jml_single_3');
			$jml_O_double 	= $this->input->post('jml_double_3');
			$jml_AB_single 	= $this->input->post('jml_single_4');
			$jml_AB_double	= $this->input->post('jml_double_4');

			$send_data_A_single 	= 'ok';
			$send_data_A_double 	= 'ok';
			$send_data_B_single 	= 'ok';
			$send_data_B_double 	= 'ok';
			$send_data_O_single 	= 'ok';
			$send_data_O_double 	= 'ok';
			$send_data_AB_single 	= 'ok';
			$send_data_AB_double	= 'ok';

			$this->db->trans_begin();
			
			//ambil URL dari berdasarkan request_id
			$url = $this->pemesanan->get_url($request_id);

			$client = new nusoap_client($url->consumer_url);
			
			// $error 	= $client->getError();
			// if ($error) {
			// 	echo 'error : '.$error;
			// }

			//memanggil service pada sistem BDRS
			$data 	= array('kode_pesan' => $request_id, 'status' => 'sent', 'is_complete' => $is_complete);
			//mengubah status permintaan darah & mengirim jumlah data darah 
			$result = $client->call('ubah_status_pengiriman', $data);

			if ($client->fault) {
				echo 'fault : '.$result;
				echo '<h2>Request</h2>';
				echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
				echo '<h2>Response</h2>';
				echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
			}
			else {
				//cecking kedua
				$error = $client->getError();
				if ($error) {
					echo 'error : '.$error;

					echo '<h2>Request</h2>';
					echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					echo '<h2>Response</h2>';
					echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
				}
				else {
					if ($result == 'ok') {

						if ($jml_A_single < 0) {
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Jumlah A single kurang dari 0, harap ulangi kembali ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}
						
						if ($jml_A_double < 0) {
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Jumlah A double kurang dari 0, harap ulangi kembali ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}
						
						if ($jml_B_single < 0) {
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Jumlah B single kurang dari 0, harap ulangi kembali ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}
						
						if ($jml_B_double < 0) {
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Jumlah B double kurang dari 0, harap ulangi kembali ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}
						
						if ($jml_O_single < 0) {
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Jumlah 0 single kurang dari 0, harap ulangi kembali ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}
						
						if ($jml_O_double < 0) {
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Jumlah O double kurang dari 0, harap ulangi kembali ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}
						
						if ($jml_AB_single < 0) {
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Jumlah AB single kurang dari 0, harap ulangi kembali ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}
						
						if ($jml_AB_double < 0) {
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Jumlah AB double kurang dari 0, harap ulangi kembali ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}
						
						//mengubah status pada tabel "request"
						$edit_local_status 	= $this->pemesanan->change_status($request_id, 'sent');
						
						//mengurangi jumlah darah pada tabel "total_stock" dan "pocket_stock"
						$update_stock_blood = $this->pemesanan->update_stock();

						//menyimpan data inputan form ke dalam tabel "receipt_request" dan tabel "receipt_pocket"
						$add_receipt 		= $this->pemesanan->create_new_receipt($request_id);

						/*mengirim data detail_pengiriman ke bdrs yg meminta*/
						$send_data_shipping_A_single 	= $this->send_detail_shipping($request_id, 'WB', 'A', 'Single', $jml_A_single);
						$send_data_shipping_A_double 	= $this->send_detail_shipping($request_id, 'WB', 'A', 'Double', $jml_A_double);
						$send_data_shipping_B_single 	= $this->send_detail_shipping($request_id, 'WB', 'B', 'Single', $jml_B_single);
						$send_data_shipping_B_double 	= $this->send_detail_shipping($request_id, 'WB', 'B', 'Double', $jml_B_double);
						$send_data_shipping_O_single 	= $this->send_detail_shipping($request_id, 'WB', 'O', 'Single', $jml_O_single);
						$send_data_shipping_O_double 	= $this->send_detail_shipping($request_id, 'WB', 'O', 'Double', $jml_O_double);
						$send_data_shipping_AB_single 	= $this->send_detail_shipping($request_id, 'WB', 'AB', 'Single', $jml_AB_single);
						$send_data_shipping_AB_double 	= $this->send_detail_shipping($request_id, 'WB', 'AB', 'Double', $jml_AB_double);

						/*
						memindahkan data darah dari tabel "detail_stock" ke tabel "detail_blood_exit" 
						(disertai log) dan mengirimkan darah tersebut ke BDRS yang meminta darah
						*/
						$send_data_A_single 	= $this->send_blood_data($request_id, 'Whole Blood', 'A', 'Single', $jml_A_single);
						$send_data_A_double 	= $this->send_blood_data($request_id, 'Whole Blood', 'A', 'Double', $jml_A_double);
						$send_data_B_single 	= $this->send_blood_data($request_id, 'Whole Blood', 'B', 'Single', $jml_B_single);
						$send_data_B_double 	= $this->send_blood_data($request_id, 'Whole Blood', 'B', 'Double', $jml_B_double);
						$send_data_O_single 	= $this->send_blood_data($request_id, 'Whole Blood', 'O', 'Single', $jml_O_single);
						$send_data_O_double 	= $this->send_blood_data($request_id, 'Whole Blood', 'O', 'Double', $jml_O_double);
						$send_data_AB_single 	= $this->send_blood_data($request_id, 'Whole Blood', 'AB', 'Single', $jml_AB_single);
						$send_data_AB_double 	= $this->send_blood_data($request_id, 'Whole Blood', 'AB', 'Double', $jml_AB_double);

						/*
							Mengecek Status Ketika Sudah Dijalankan Semua
						*/
						if ($edit_local_status != 'ok') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Edit local status pemesanan gagal dilakukan ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

						if ($update_stock_blood != 'ok5') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Update jumlah stock gagal dilakukan ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

						if ($add_receipt != 'ok1234') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Pembuatan receipt dilakukan ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

						if ($send_data_shipping_A_single != 'ok') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Kirim data detail pengiriman A single gagal dilakukan ! , '.$send_data_shipping_A_single.' <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

						// echo $send_data_shipping_A_double;

						if ($send_data_shipping_A_double != 'ok') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Kirim data detail pengiriman A double gagal dilakukan ! , '.$send_data_shipping_A_double.' <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

						if ($send_data_shipping_B_single != 'ok') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Kirim data detail pengiriman B single gagal dilakukan ! , '.$send_data_shipping_B_single.' <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

						if ($send_data_shipping_B_double != 'ok') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Kirim data detail pengiriman B double gagal dilakukan ! , '.$send_data_shipping_B_double.' <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

						if ($send_data_shipping_O_single != 'ok') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Kirim data detail pengiriman O single gagal dilakukan ! , '.$send_data_shipping_O_single.' <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

						if ($send_data_shipping_O_double != 'ok') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Kirim data detail pengiriman O double gagal dilakukan ! , '.$send_data_shipping_O_double.' <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

						if ($send_data_shipping_AB_single != 'ok') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Kirim data detail pengiriman AB single gagal dilakukan ! , '.$send_data_shipping_AB_single.' <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

						if ($send_data_shipping_AB_double != 'ok') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Kirim data detail pengiriman AB single gagal dilakukan ! , '.$send_data_shipping_AB_double.' <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}


						if ($send_data_A_single != 'ok') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Kirim data A single gagal dilakukan ! , '.$send_data_A_single.' <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

						echo $send_data_A_double;

						if ($send_data_A_double != 'ok') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Kirim data A double gagal dilakukan ! , '.$send_data_A_double.' <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

						if ($send_data_B_single != 'ok') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Kirim data B single gagal dilakukan ! , '.$send_data_B_single.' <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

						if ($send_data_B_double != 'ok') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Kirim data B double gagal dilakukan ! , '.$send_data_B_double.' <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

						if ($send_data_O_single != 'ok') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Kirim data O single gagal dilakukan ! , '.$send_data_O_single.' <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

						if ($send_data_O_double != 'ok') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Kirim data O double gagal dilakukan ! , '.$send_data_O_double.' <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

						if ($send_data_AB_single != 'ok') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Kirim data AB single gagal dilakukan ! , '.$send_data_AB_single.' <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

						if ($send_data_AB_double != 'ok') {
							$this->db->trans_rollback();
							
							$this->session->set_flashdata('error','<div class="alert alert-danger text-center"><i class="fa fa-hand-paper-o"></i> Kirim data AB double gagal dilakukan ! , '.$send_data_AB_double.' <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
							
							redirect('pemesanan');
						}

							
						$this->session->set_flashdata('success','<div class="alert alert-success text-center"><i class="fa fa-hand-paper-o"></i> Pengiriman data berhasil dilakukan ! <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
						
						redirect('pemesanan');														
						
					}
					else {
						$this->db->trans_rollback();

						$this->session->set_flashdata('error','<div class="alert alert-info text-center"><i class="fa fa-hand-paper-o"></i> Status pemesanan gagal dirubah ! (service) <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
						
						redirect('pemesanan');
					} //close else($result == 'ok')

				} //close else(checking service)

			}

			// echo '<h2>Response</h2>';
			// echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';


		}

		
	}

	private function send_blood_data($request_id, $category_name, $type_name, $pocket_name, $jumlah)
	{
		// //mengatur maximum execution time menjadi 5 menit
		ini_set('max_execution_time', 300);

		$this->load->model('Pemesanan_model', 'pemesanan');

		$url = $this->pemesanan->get_url($request_id);

		$flag 	= 0;
		$error 	= '';
		
		//mengambil data dari tabel "detail_stock"
		$get_data_darah = $this->pemesanan->get_data_detail_stock($category_name, $type_name, $pocket_name, $jumlah);
		
		$client = new nusoap_client($url->consumer_url);
		
		$error = $client->getError();
		if ($error) {
			echo 'error connect : '.$error;
		}

		if ($jumlah == 0) {
			//mengecek log pada tabel "exit_blood" dan "exit_pocket"
			$check_exit_blood 	= $this->pemesanan->check_exit_blood($request_id, $category_name, $type_name);
			$check_exit_pocket 	= $this->pemesanan->check_exit_pocket($request_id, $category_name, $type_name, $pocket_name);

			if ( ($check_exit_blood == 'kosong') && ($check_exit_pocket == 'kosong') ) {
				//membuat log darah keluar ke tabel "exit_blood"
				$add_log_exit_blood = $this->pemesanan->create_new_exit_blood($request_id, $category_name, $type_name, '0');
				
				//membuat log darah keluar perkantung ke tabel "exit_pocket"
				$add_log_exit_pocket = $this->pemesanan->create_new_exit_pocket($request_id, $category_name, $type_name, $pocket_name, '0');
			}
			else if ( ($check_exit_blood == 'ada') && ($check_exit_pocket == 'kosong') ) {
				//membuat log darah keluar perkantung ke tabel "exit_pocket"
				$add_log_exit_pocket = $this->pemesanan->create_new_exit_pocket($request_id, $category_name, $type_name, $pocket_name, '0');
			}

			$update_status = $this->pemesanan->update_recpocket_status_receipt_pocket($request_id, $category_name, $type_name, $pocket_name);

			if ($update_status == 'ok') {
				return 'ok';
			}
			else {
				$error = 'gagal mengupdate status recpocket_status';	
				return $error;
			} //close else '$update_status'

		} //close if($jumlah == '0')
		else if ($jumlah > 0) {
			foreach ($get_data_darah as $key => $value) {

				//mengecek log pada tabel "exit_blood" dan "exit_pocket"
				$check_exit_blood 	= $this->pemesanan->check_exit_blood($request_id, $value->category_name, $value->type_name);
				$check_exit_pocket 	= $this->pemesanan->check_exit_pocket($request_id, $value->category_name, $value->type_name, $value->pocket_name);

				//Data darah yang akan dikirim ke BDRS
				$data_darah = array(
									'no_barcode' 		=> $value->stock_barcode,
									'tgl_aftap'			=> $value->stock_date_aftap,
									'komponen_darah'	=> $value->category_symbol,
									'jenis_kantung'		=> $value->pocket_name,
									'golongan_darah'	=> $value->type_name,
									'rhesus'			=> $value->stock_rhesus,
									'tgl_expire'		=> $value->stock_date_expired
								);

				$data_kirim = array(
									array(
										'pemesanan_kode'	=> $request_id,
										'data_darah' 	 	=> $data_darah	
									) 
								);

				//data darah yang akan dipindahkan dari tabel "detail_stock" ke table "detail_blood_exit"
				$data_darah_in	= array(
									'request_id'			=> $request_id,
									'stockout_barcode'		=> $value->stock_barcode,
									'stockout_date_aftap' 	=> $value->stock_date_aftap,
									'category_id'			=> $value->category_id,
									'pocket_id'				=> $value->pocket_id,
									'type_id'				=> $value->type_id,
									'stockout_rhesus'		=> $value->stock_rhesus,
									'stockout_date_expired'	=> $value->stock_date_expired
								);

				if ( ($check_exit_blood == 'kosong') && ($check_exit_pocket == 'kosong') ) {
					
					//membuat log darah keluar ke tabel "exit_blood"
					$add_log_exit_blood = $this->pemesanan->create_new_exit_blood($request_id, $value->category_name, $value->type_name, '1');
					
					if ($add_log_exit_blood == 'ok') {
						
						//membuat log darah keluar perkantung ke tabel "exit_pocket"
						$add_log_exit_pocket = $this->pemesanan->create_new_exit_pocket($request_id, $value->category_name, $value->type_name, $value->pocket_name, '1');
						
						if ($add_log_exit_pocket == 'ok') {
							
							//menambahkan data ke table "detail_blood_exit"
							$add_detail_blood_exit = $this->pemesanan->add_data_detail_blood_exit($data_darah_in);

							if ($add_detail_blood_exit == 'ok') {
								
								//menghapus data dari "detail_stock"
								$delete_detail_stock = $this->pemesanan->delete_data_detail_stock($value->stock_id);

								if ($delete_detail_stock == 'ok') {

									//mengirim data ke BDRS yang meminta darah
									$send_data = $client->call('input_persediaan', $data_kirim);
									
									if ($client->fault) {
										$error = 'fault : '.$send_data;
										// print_r($send_data);
									}
									else {
										//cecking kedua
										$error = $client->getError();
										
										if ($error) {
											$error = 'error : '.$error;
										}
										else {
											if ($send_data == 'ok') {
												$flag++;
											}
											else {
												$error = 'cancel 5 [kosong, kosong]';
												break;
											} //close else "$send_data"

										} //close else "$error"

									} //close else "$client->fault"
									
								} //close if "$delete_detail_stock"
								else {
									// echo 'gagal menghapus data dari detail_stock <br/>';
									$error = 'cancel 4 [kosong, kosong]';
									break;
									
								} //close else "$delete_detail_stock"

							} //close if "$add_detail_blood_exit"
							else {
								$error = 'cancel 3 [kosong, kosong]';
								break;
								// echo 'gagal menginput ke detail_blood_exit <br/>';
							
							} //close else "$add_detail_blood_exit"

						} //close if "$add_log_exit_pocket"
						else {
							$error = 'cancel 2 [kosong, kosong]';
							break;
							// echo 'gagal membuat log exit_pocket <br/>';
						
						} //close else "$add_log_exit_pocket"

					} //close if "$add_log_exit_blood"
					else {
						// echo 'gagal membuat log exit_blood <br/>';
						$error = 'cancel 1 [kosong, kosong]';
						break;

					} //close else "$add_log_exit_blood"
				} //close if( ($check_exit_blood == 'kosong') && ($check_exit_pocket == 'kosong') )

				else if ( ($check_exit_blood == 'ada') && ($check_exit_pocket == 'kosong') ) {
					
					//mengupdate log darah keluar ke tabel "exit_blood"
					$update_log_exit_blood = $this->pemesanan->update_exit_total_exit_blood($request_id, $value->category_name, $value->type_name);
					
					if ($update_log_exit_blood == 'ok') {
						
						//membuat log darah keluar perkantung ke tabel "exit_pocket"
						$add_log_exit_pocket = $this->pemesanan->create_new_exit_pocket($request_id, $value->category_name, $value->type_name, $value->pocket_name, '1');

						if ($add_log_exit_pocket == 'ok') {
							
							//menambahkan data ke table "detail_blood_exit"
							$add_detail_blood_exit = $this->pemesanan->add_data_detail_blood_exit($data_darah_in);

							if ($add_detail_blood_exit == 'ok') {
								
								//menghapus data dari "detail_stock"
								$delete_detail_stock = $this->pemesanan->delete_data_detail_stock($value->stock_id);

								if ($delete_detail_stock == 'ok') {

									//mengirim data ke BDRS yang meminta darah
									$send_data = $client->call('input_persediaan', $data_kirim);
									
									if ($client->fault) {
										$error = 'fault : '.$send_data;
										// print_r($send_data);
									}
									else {
										//cecking kedua
										$error = $client->getError();
										
										if ($error) {
											$error = 'error : '.$error;
										}
										else {
											if ($send_data == 'ok') {
												// $this->db->trans_commit();
												$flag++;
											}
											else {
												$error = 'cancel 5 [ada, kosong]';
												break;
											} //close else "$send_data"

										} //close else "$error"

									} //close else "$client->fault"

								} //close if "$delete_detail_stock"
								else {
									// echo 'gagal menghapus data dari detail_stock <br/>';
									$error = 'cancel 4 [ada, kosong]';
									break;
									
								} //close else "$delete_detail_stock"

							} //close if "$add_detail_blood_exit"
							else {
								// echo 'gagal menginput ke detail_blood_exit <br/>';
								$error = 'cancel 3 [ada, kosong]';
								break;
								

							} //close else "$add_detail_blood_exit"

						} //close if "$add_log_exit_pocket"
						else {
							// echo 'gagal membuat log exit_pocket <br/>';
							$error = 'cancel 2 [ada, kosong]';
							break;
							

						} //close else "$add_log_exit_pocket"

					} //close if "$update_log_exit_blood"
					else {
						// echo 'gagal mengupdate log exit_blood <br/>';
						$error = 'cancel 1 [ada, kosong]';
						break;
						

					} //close else "$update_log_exit_blood"
				} //close else if( ($check_exit_blood == 'ada') && ($check_exit_pocket == 'kosong') )

				else if ( ($check_exit_blood == 'ada') && ($check_exit_pocket == 'ada') ) {
					
					//mengupdate log darah keluar ke tabel "exit_blood"
					$update_log_exit_blood = $this->pemesanan->update_exit_total_exit_blood($request_id, $value->category_name, $value->type_name);
					
					if ($update_log_exit_blood == 'ok') {
						
						//mengupdate log darah keluar perkantung ke tabel "exit_pocket"
						$update_log_exit_pocket = $this->pemesanan->update_exit_pockettotal_exit_pocket($request_id, $value->category_name, $value->type_name, $value->pocket_name);
						
						if ($update_log_exit_pocket == 'ok') {
							
							//menambahkan data ke table "detail_blood_exit"
							$add_detail_blood_exit = $this->pemesanan->add_data_detail_blood_exit($data_darah_in);

							if ($add_detail_blood_exit == 'ok') {
								
								//menghapus data dari "detail_stock"
								$delete_detail_stock = $this->pemesanan->delete_data_detail_stock($value->stock_id);

								if ($delete_detail_stock == 'ok') {
									// echo 'ok <br/>';
									
									// //mengirim data ke BDRS yang meminta darah
									$send_data = $client->call('input_persediaan', $data_kirim);
									
									if ($client->fault) {
										$error = 'fault : '.$send_data;
										// print_r($send_data);
									}
									else {
										//cecking kedua
										$error = $client->getError();
										
										if ($error) {
											$error = 'error : '.$error;
										}
										else {
											if ($send_data == 'ok') {
												// $this->db->trans_commit();
												$flag++;
											}
											else {
												$error = 'cancel 5 [ada, ada]';
												break;
											} //close else "$send_data"

										} //close else "$error"

									} //close else "$client->fault"

								}
								else {
									// echo 'gagal menghapus data dari detail_stock <br/>';
									$error = 'cancel 4 [ada, ada]';
									break;
								}

							} //close if "$add_detail_blood_exit"
							else {
								// echo 'gagal menginput ke detail_blood_exit <br/>';
								$error = 'cancel 3 [ada, ada]';
								break;

							} //close else "$add_detail_blood_exit"

						} //close if "$update_log_exit_pocket"
						else {
							// echo 'gagal mengupdate log exit_pocket <br/>';
							$error = 'cancel 2 [ada, ada]';
							break;

						} //close else "$update_log_exit_pocket"

					} //close if "$update_log_exit_blood"
					else {
						$error = 'cancel 1 [ada, ada]';
						break;
						
						// echo 'gagal mengupdate log exit_blood <br/>';
					} //close else "$update_log_exit_blood"

				} //close else if( ($check_exit_blood == 'ada') && ($check_exit_pocket == 'ada') )

				else {
					// echo 'terjadi kesalahan <br/>';
					$error = 'cancel semua';
					break;
					
				}

			} //end foreach

			$update_status = $this->pemesanan->update_recpocket_status_receipt_pocket($request_id, $category_name, $type_name, $pocket_name);

			if ($update_status == 'ok') {
				return 'ok';
			}
			else {
				$error = 'gagal mengupdate status recpocket_status';	
				return $error;
			} //close else '$update_status'

		} //close else if($jumlah > 0)
		else {
			$error = 'gagal mengupdate status recpocket_status';	
			return $error;
		}
		
	}

	public function show_sent()
	{
		$this->load->model('Pemesanan_model', 'pemesanan');

		$data['pemesanan_terkirim'] = $this->pemesanan->get_pemesanan_terkirim();

		$this->load->view('pemesanan/show_terkirim', $data);
	}

	public function show_done()
	{
		$this->load->model('Pemesanan_model', 'pemesanan');
		
		$data['pemesanan_done']	= $this->pemesanan->get_pemesanan_selesai();

		$this->load->view('pemesanan/show_done', $data);
	}

	public function show_detail_sent($request_id)
	{
		$this->load->model('Pemesanan_model', 'pemesanan');

		$data_pemesanan 		= $this->pemesanan->detail_request($request_id);
		$detail_data_pemesanan 	= $this->pemesanan->detail_sent($request_id);

		if (empty($data_pemesanan) && empty($detail_data_pemesanan)) {
			$this->session->set_flashdata('error','<div class="alert alert-warning text-center"><i class="fa  fa-warning "></i> <strong>Detail Pemesanan Darah Yang Anda Cari Tidak Tersedia</strong> <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
			redirect('pemesanan/show_sent');
		}
		else {
			$data['detail_pemesanan'] 	= $data_pemesanan;
			$data['detail_category']	= $this->pemesanan->detail_category($request_id);
			//detail data pemesanan darah
			$data['detail_sent']		= $detail_data_pemesanan; 

			$this->load->view('pemesanan/show_sent_detail', $data);
		}

	}

	public function show_detail_done($request_id)
	{
		$this->load->model('Pemesanan_model', 'pemesanan');

		$data_pemesanan 		= $this->pemesanan->detail_request($request_id);
		$detail_data_pemesanan 	= $this->pemesanan->detail_sent($request_id);

		if (empty($data_pemesanan) && empty($detail_data_pemesanan)) {
			$this->session->set_flashdata('error','<div class="alert alert-warning text-center"><i class="fa  fa-warning "></i> <strong>Detail Pemesanan Darah Yang Anda Cari Tidak Tersedia</strong> <a href="#" class="close" style="text-decoration : none;" data-dismiss="alert" aria-label="close">&times;</a></div>');
			redirect('pemesanan/show_done');
		}
		else {
			$data['detail_pemesanan'] 	= $data_pemesanan;
			$data['detail_category']	= $this->pemesanan->detail_category($request_id);
			//detail data pemesanan darah
			$data['detail_sent']		= $detail_data_pemesanan; 

			$this->load->view('pemesanan/show_done_detail', $data);
		}

	}

	private function send_detail_shipping($request_id, $category_symbol, $type_name, $pocket_name, $total)
	{
		$error 	= '';
		$url 	= $this->pemesanan->get_url($request_id);
		$client = new nusoap_client($url->consumer_url);

		if ($total == 'kosong') {
			$total = 0;
		}

		$error 	= $client->getError();
		
		if ($error) {
			echo 'error connect : '.$error;
		}

		$data_darah = array(
						array(
							'permintaan_kode' 	=> $request_id,
							'komponen_darah'	=> $category_symbol,
							'golongan_darah'	=> $type_name,
							'jenis_kantung'		=> $pocket_name,
							'total'				=> $total
						)
					);

		//mengirim data ke BDRS yang meminta darah
		$send_data = $client->call('input_detail_pengiriman', $data_darah);
		
		if ($client->fault) {
			$error = 'fault : '.$send_data;
			echo '<h2>Request</h2>';
			echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
			echo '<h2>Response</h2>';
			echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
			// // print_r($send_data);
		}
		else {
			//cecking kedua
			$error = $client->getError();
			
			if ($error) {
				$error = 'error : '.$error;
				echo '<h2>Request</h2>';
				echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
				echo '<h2>Response</h2>';
				echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
			}
			else {
				if ($send_data == 'ok') {
					return 'ok';
				}
				else {
					// $error = 'detail pengiriman '.$category_symbol.' golongan '.$type_name.' gagal terkirim';
					// echo '<h2>Request</h2>';
					// echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
					// echo '<h2>Response</h2>';
					// echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
					// return $error;
					echo $send_data;
				} //close else "$send_data"

			} //close else "$error"

		} //close else "$client->fault"

	}

	public function export_pdf($request_id)
	{
		$this->load->library('PdfGenerator');

		$this->load->model('Pemesanan_model', 'pemesanan');

		$data_pemesanan 		= $this->pemesanan->detail_request($request_id);
		$detail_data_pemesanan 	= $this->pemesanan->detail_sent($request_id);

		$data['pemesanan']			= $this->pemesanan->detail_request($request_id);
		$data['detail_pemesanan']	= $this->pemesanan->detail_sent($request_id);
		
		// $html = $this->load->view('permintaan/template_export', $data, true);

		$html = $this->load->view('pemesanan/template_export', $data, true);

		$this->pdfgenerator->generate($html, 'formulir penerimaan');
	}

	public function sample($var = '')
	{
		/*
		link trigger :
		https://www.postgresql.org/docs/9.1/static/sql-createtrigger.html
		http://www.thegeekstuff.com/2010/07/8-postgresql-date-and-time-function-examples/

		link pdf generator :
		http://agung-setiawan.com/cetak-pdf-dari-html-pada-codeigniter-3-0/
		*/

		// echo '<h2>Request</h2>';
		// echo '<pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
		// echo '<h2>Response</h2>';
		// echo '<pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';

		// 	// Display the debug messages
		// echo '<h2>Debug</h2>';
		// echo '<pre>' . htmlspecialchars($client->debug_str, ENT_QUOTES) . '</pre>';
		$sample_variable = '';

		$this->load->model('Pemesanan_model', 'pemesanan');

		//$send_data_A_single 	= $this->send_blood_data('rs1201610211133300021', 'Whole Blood', 'A', 'Single', 2);
		// $coba = $this->pemesanan->get_receipt_id_receipt_request('rs1201610211133300021', 'Whole Blood', 'A');
		echo $this->pemesanan->get_category_id('Whole Blood');

		// $sample_var = 0;
		// echo is_numeric($sample_var);
		// if (is_numeric($sample_var)) {
		// 	echo "ini angka";
		// }
		// else {
		// 	echo "ini bukan angka";
		// }

		/*
		error karena int tidak bisa menerima inputan dengan nilai 0 (unsigned zero value) pada service subsistem UDD
		*/
	}
	/*
	Pengaturan SOAP di PHP Ubuntu
	http://php.net/manual/en/soap.configuration.php
	*/

}

/* End of file Pemesanan.php */
/* Location: ./application/controllers/Pemesanan.php */