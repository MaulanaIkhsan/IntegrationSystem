<!DOCTYPE html>
<html ng-app>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Sistem UDD</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.6 -->
	<link rel="stylesheet" href="<?php echo base_url('assets/adminLTE/bootstrap/css/bootstrap.min.css'); ?>">
	<!-- Self modification -->
	<link rel="stylesheet" href="<?php echo base_url('assets/self/css/core-adminLTE.css');?>">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url('assets/adminLTE/FontAwesome/css/font-awesome.min.css'); ?>">
	 
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url('assets/adminLTE/dist/css/AdminLTE.min.css');?>" />
	<!-- AdminLTE Skins. Choose a skin from the css/skins
	       folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?php echo base_url('assets/adminLTE/dist/css/skins/_all-skins.min.css'); ?>">
	<!-- Own style -->
	<link rel="stylesheet" href="<?php echo base_url('assets/self/css/core-adminLTE.css'); ?>">

	<script src="<?php echo base_url('assets/adminLTE/plugins/jQuery/jQuery-2.2.0.min.js'); ?>"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> -->
