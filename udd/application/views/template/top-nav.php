</head>
<body class="hold-transition skin-red sidebar-mini fixed">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo site_url('beranda'); ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>menu</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>UDD</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li>
            <a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-sign-out"></i> Keluar</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

<!-- Modal konfirmasi keluar -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> Konfirmasi keluar</h4>
      </div>
      <div class="modal-body">
        Keluar dari sistem ?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
        <!-- <button type="button" class="btn btn-primary"></button> -->
        <a href="<?php echo site_url('login/logout'); ?>" class="btn btn-primary"><i class="fa fa-check"></i> Ya, keluar</a>
      </div>
    </div>
  </div>
</div>