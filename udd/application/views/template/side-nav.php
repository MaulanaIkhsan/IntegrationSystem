<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url('assets/self/img/user.png'); ?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info" id="own-info">
        <p><?php echo $this->session->userdata('nama_udd'); ?></p>
        
      </div>
    </div>
   
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header" id="nav-header">DAFTAR MENU</li>
      <li>
        <a href="<?php echo site_url('beranda'); ?>">
          <i class="fa fa-home"></i><span>Beranda</span></i>
        </a>
      </li>
      <li>
        <a href="<?php echo site_url('persediaan'); ?>">
          <i class="fa fa-files-o"></i>
          <span>Persediaan</span>
        </a>
      </li>
      <li>
        <a href="#">
          <i class="fa fa-paste"></i> 
          <span>Pemesanan</span> 
          <!-- masuk, terkirim, selesai -->
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li>
            <a href="<?php echo site_url('pemesanan'); ?>"><i class="fa fa-file-o"></i> Masuk  </a>
            
          </li>
          <li>
            <a href="<?php echo site_url('pemesanan/show_sent'); ?>"><i class="fa fa-file-o"></i> Terkirim
            </a>
          </li>
          <li><a href="<?php echo site_url('pemesanan/show_done'); ?>"><i class="fa fa-history"></i> Selesai</a></li>
        </ul>
      </li>
     
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>