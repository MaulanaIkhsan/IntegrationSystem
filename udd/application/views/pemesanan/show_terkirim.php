<?php $this->load->view('template/header'); ?>

<!-- Tempat meletakkan plugin pada bagian header -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/css/dataTables.bootstrap.min.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatable/css/jquery.dataTables.min.css'); ?>" />

<?php $this->load->view('template/top-nav'); ?>
<?php $this->load->view('template/side-nav'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="col-md-12"><?php echo $this->session->flashdata('success'); ?></div>
      <div class="col-md-12"><?php echo $this->session->flashdata('error'); ?></div>
      
      <!-- Main row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-file-o"></i> <strong>Pemesanan Darah Terkirim</strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-12">&nbsp;</div>
              <div class="col-md-12">
                <table id="tabelku">
                  <thead>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Instansi</center></th>
                      <th><center>Tanggal Pesan</center></th>
                      <th><center>Jam</center></th>
                      <th><center>Petugas</center></th>
                      <th><center>Total</center></th>
                      <th><center>Pengiriman</center></th>
                      <th><center>Status</center></th>
                      <th><center>Operasi</center></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $no = 1;
                    if (is_array($pemesanan_terkirim)) {
                       foreach ($pemesanan_terkirim as $value) { 
                        ?>
                          <tr>
                            <td><center><?php echo $no; ?></center></td>
                            <td><center><?php echo $value->consumer_name; ?></center></td>
                            <td><center><?php echo date("d-M-Y", strtotime($value->request_date)); ?></center></td>
                            <td><center><?php echo $value->request_time; ?></center></td>
                            <td><center><?php echo $value->consumer_employee; ?></center></td>
                            <td><center><?php echo $value->request_total; ?></center></td>
                            <td><center><?php echo $value->request_status; ?></center></td>
                            <td><center>
                              <?php if ($value->request_total < 50) { ?>
                                <small class="label bg-green">Mandiri</small>
                              <?php } else { ?>
                                <small class="label bg-yellow">Antar</small>
                              <?php } ?>
                            </center></td>
                            <td>
                              <center>
                                <a href=<?php echo site_url('pemesanan/show_detail_sent/'.$value->request_id); ?> class="btn btn-primary"><i class="fa fa-question"></i> Detail</a> | 
                                <a href="<?php echo site_url('pemesanan/export_pdf/'.$value->request_id)?>" class="btn btn-warning" target="blank"><i class="fa fa-file-pdf-o"></i> Formulir</a>
                              </center>
                            </td>
                          </tr>

                      <?php 
                      $no++;
                      }
                    } 
                    else {
                      echo '<tr><td colspan=9><center><strong>---- '.$pemesanan_terkirim.' ----</strong></center></td></tr>';
                    } 
                    ?>
                  </tbody>
                </table>
              </div>
            </div> <!-- close box-body -->
          </div> <!-- close box-success -->
        </div> <!-- close col-md-12 -->
      </div> <!-- close row -->
    </section>
    <!-- /.content -->
  </div>
  
  <?php $this->load->view('template/info-footer.php'); ?>

</div>
<!-- ./wrapper -->

<!-- Tempat meletakkan plugin pada bagian footer -->
<script type="text/javascript" src="<?php echo base_url('assets/datatable/js/jquery.dataTables.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/datatable/js/dataTables.bootstrap.min.js') ?>"></script>
<script type="text/javascript">
  $(document).ready(function(){
    var data_persediaan = $("#tabelku").DataTable({
                            "pageLength": 25
                          });

    setInterval(function test(){
      data_persediaan.fnDraw();
    }, 1000);
  });
</script>

<?php $this->load->view('template/footer'); ?>
