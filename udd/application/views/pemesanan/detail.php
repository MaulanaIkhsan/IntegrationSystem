<?php $this->load->view('template/header'); ?>

<!-- Tempat meletakkan plugin pada bagian header -->

<?php $this->load->view('template/top-nav'); ?>
<?php $this->load->view('template/side-nav'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="col-md-12"><?php echo $this->session->flashdata('success'); ?></div>
      <div class="col-md-12"><?php echo $this->session->flashdata('error'); ?></div>
      
      <!-- Main row -->
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-question"></i> <strong>Detail Pemesanan Darah</strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-5">
              	<div class="pull-right">Tanggal</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
              	<div class="pull-left"><?php echo date("d-M-Y", strtotime($detail_pemesanan->request_date)); ?></div>
              </div> 
              <div class="col-md-5">
                <div class="pull-right">Jam</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
                <div class="pull-left"><?php echo $detail_pemesanan->request_time; ?></div>
              </div> 
              <div class="col-md-5">
              	<div class="pull-right">Instansi</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
              	<div class="pull-left"><?php echo $detail_pemesanan->consumer_name; ?></div>
              </div>
              <div class="col-md-5">
                <div class="pull-right">Nama Petugas</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
                <div class="pull-left"><?php echo $detail_pemesanan->consumer_employee; ?></div>
              </div> 
              <div class="col-md-5">
              	<div class="pull-right">Status</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
              	<div class="pull-left"><?php echo $detail_pemesanan->request_status; ?></div>
              </div>
              <div class="col-md-5">
                <div class="pull-right">Pengiriman</div>
              </div>
              <div class="col-md-1">:</div>
              <div class="col-md-5">
                <div class="pull-left">
                  <?php if ($detail_pemesanan->request_total <= 50) { ?>
                  <small class="label bg-green">Mandiri</small>
                  <?php } else { ?>
                  <small class="label bg-yellow">Antar</small>
                  <?php } ?>
                </div>
              </div>
              <div class="col-md-12">&nbsp;</div>
              <div class="col-md-5">
              	<div class="pull-right">Komponen : <?php echo $detail_category->category_name; ?></div>
              </div>
              <div class="col-md-12">&nbsp;</div>
              <div class="col-md-8 col-md-offset-2">
              	<table>
                  <thead>
                    <tr>
                      <th><center>Golongan Darah</center></th>
                      <th><center>Jumlah</center></th>
                    </tr>
                  </thead>
              		<tbody>
                    <?php foreach ($detail_type as $value) { ?>
                      <tr>
                        <td><center><?php echo $value->type_name; ?></center></td>
                        <td><div class="pull-right"><?php echo $value->detail_total; ?></div></center></td>
                      </tr>
                    <?php } ?>
                  </tbody>
              		
              		<tr>
              			<td><center>Total</center></td>
              			<td><div class="pull-right"><?php echo $detail_pemesanan->request_total; ?></div></td>
              		</tr>
              	</table>
              </div> 
            </div> <!-- close box-body -->
            <div class="box-footer">
            	<a href="<?php echo site_url('pemesanan'); ?>" class="btn btn-danger pull-left"><i class="fa fa-backward"></i> Kembali</a>
            	<div class="pull-right">
            		<a href="#" data-toggle="modal" data-target="#modal_konfirm" class="btn btn-success"><i class="fa fa-hand-o-right"></i> Proses</a>
            	
                        <!-- konfirm dialog -->

                          <div class="modal fade" id="modal_konfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel"><i class="fa fa-warning"></i> <strong>Konfirmasi Proses</strong></h4>
                                </div>
                                <div class="modal-body">
                                  Apakah anda ingin memproses pemesanan tgl "<?php echo date("d-M-Y", strtotime($detail_pemesanan->request_date)); ?>" dari  "<?php echo $detail_pemesanan->consumer_name; ?>" ?
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Tidak</button>
                                  <!-- <button type="button" class="btn btn-primary"></button> -->
                                  <a href="<?php echo site_url('pemesanan/execute_request/'.$detail_pemesanan->request_id); ?>" class="btn btn-primary"><i class="fa fa-check"></i> Ya, Proses</a>
                                </div>
                              </div>
                            </div>
                          </div>

              </div>
            </div>
          </div> <!-- close box-success -->
        </div> <!-- close col-md-12 -->
      </div> <!-- close row -->
    </section>
    <!-- /.content -->
  </div>
  
  <?php $this->load->view('template/info-footer.php'); ?>

</div>
<!-- ./wrapper -->

<!-- Tempat meletakkan plugin pada bagian footer -->

<?php $this->load->view('template/footer'); ?>
