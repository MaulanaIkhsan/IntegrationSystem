<?php $this->load->view('template/header'); ?>

<!-- Tempat meletakkan plugin pada bagian header -->

<?php $this->load->view('template/top-nav'); ?>
<?php $this->load->view('template/side-nav'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    
    <!-- Main content -->
    <section class="content">
      
      <!-- Main row -->
      <div class="row">
        <div class="col-md-12"><?php echo $this->session->flashdata('success'); ?></div>
        <div class="col-md-12"><?php echo $this->session->flashdata('error'); ?></div>

        <div class="col-md-8 col-md-offset-2">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-send"></i> <strong>Pengiriman Data Darah</strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              <form action="<?php echo site_url('pemesanan/kirim_data/'.$request); ?>" method="post">
              <table>
                <thead>
                  <tr>
                    <th rowspan="2" valign="middle"><center>Golongan Darah</center></th>
                    <th rowspan="2" valign="middle"><center>Jumlah Minta</center></th>
                    <th colspan="2"><center>Kirim Perkantung</center></th>
                    <th rowspan="2" valign="middle"><center>Jumlah Kirim</center></th>
                  </tr>
                  <tr>
                    <th><center>Single</center></th>
                    <th><center>Double</center></th>
                  </tr>                  
                </thead>
                <tbody>
                  <?php 
                  $no = 1;
                  foreach ($detail_item as $value) {?>
                    <tr>
                        <td><center><?php echo $value->type_name; ?></center></td>
                        <td><center><?php echo $value->detail_total; ?></center></td>
                        <td>
                          <input type="number" name="jml_single_<?php echo $no; ?>" class="form-control" ng-model= "single_<?php echo $no; ?>" required="true" />
                          <span class="text-danger col-md-8"><?php echo form_error('jml_single_'.$no); ?></span>
                        </td>
                        <td>
                          <input type="number" name="jml_double_<?php echo $no; ?>" class="form-control" ng-model= "double_<?php echo $no; ?>" required="true" />
                          <span class="text-danger col-md-8"><?php echo form_error('jml_double_'.$no); ?></span>
                        </td>
                        <td>
                          <input type="number" name="jml_kirim_<?php echo $no; ?>" class="form-control" readonly="true" value="{{ (single_<?php echo $no; ?>*1) + (double_<?php echo $no; ?> * 2) }}" />
                        </td>
                      
                    </tr>
                  <?php 
                  $no++;
                  } ?>
                  <tr>
                    <td colspan="4" id="footer-table"><center><strong>Total Pengiriman</strong></center></td>
                    <td id="footer-table"><input type="number" class="form-control" name="jumlah_semua" readonly="true" value="{{ (single_1 + (double_1*2)) + (single_2 + (double_2*2)) + (single_3 + (double_3*2)) +(single_4 + (double_4*2)) }}" /></td>
                  </tr>
                </tbody>
              </table>
            </div> <!-- close box-body -->
            <div class="box-footer">
              <a href="<?php echo site_url('pemesanan'); ?>" class="btn btn-danger pull-left"><i class="fa fa-backward"></i> Kembali</a>
              <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-send"></i> Kirimkan Data</button>
            </div> <!-- close box-footer -->
          </div> <!-- close box -->
         </form>
        </div> <!-- close col-md-12 -->
      </div> <!-- close row -->
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  
  <?php $this->load->view('template/info-footer.php'); ?>

</div>
<!-- ./wrapper -->

<!-- Tempat meletakkan plugin pada bagian footer -->
<script type="text/javascript" src="<?php echo base_url('assets/angularJS/angular.min.js'); ?>"></script>

<?php $this->load->view('template/footer'); ?>