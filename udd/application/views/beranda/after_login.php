<?php $this->load->view('template/header'); ?>

<!-- Tempat meletakkan plugin pada bagian header -->

<?php $this->load->view('template/top-nav'); ?>
<?php $this->load->view('template/side-nav'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
      <div class="col-md-12"><?php echo $this->session->flashdata('success'); ?></div>
      <div class="col-md-12"><?php echo $this->session->flashdata('error'); ?></div>
      
      <!-- Main row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-home"></i><strong>Halaman Beranda</strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              
              <div class="row">
                <div>&nbsp;</div>
                <div class="col-md-5 col-sm-6 col-xs-12">
                  <a href="<?php echo site_url('pemesanan'); ?>">
                  <div class="info-box">
                    <span class="info-box-icon bg-red">
                      <i class="fa fa-envelope-o"></i>
                    </span>
                    <div class="info-box-content">
                      <span class="info-box-text">Pemesanan Darah Masuk</span>
                      <span class="info-box-number"><?php echo $jml_masuk->jumlah_masuk; ?></span>
                    </div>
                  </div> <!-- close info-box -->
                  </a>
                </div> <!-- close col-md-6 col-sm-6 col-xs-12 --> 
                
                <div class="col-md-5 col-sm-6 col-xs-12">
                  <a href="<?php echo site_url('pemesanan/show_sent'); ?>">
                  <div class="info-box">
                    <span class="info-box-icon bg-yellow">
                      <i class="fa fa-inbox"></i>
                    </span>
                    <div class="info-box-content">
                      <span class="info-box-text">Pemesanan Darah Terkirim</span>
                      <span class="info-box-number"><?php echo $jml_keluar->jumlah_keluar; ?></span>
                    </div>
                  </div> <!-- close info-box -->
                  </a>
                </div> <!-- close col-md-3 col-sm-6 col-xs-12 --> 
                
                <div class="col-md-12">
                  <hr class="style-one">
                </div>
                <div class="col-md-12 col-sm-6 col-xs-12">
                   <h4>Komponen Darah : <?php echo $category->category_name; ?> (<?php echo $category->category_symbol; ?>)</h4>
                </div>
                <div>&nbsp;</div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <table>
                    <thead>
                      <tr>
                        <th rowspan="2" valign="middle"><center>Golongan Darah</center></th>
                        <th colspan="2"><center>Jenis Kantung</center></th>
                        <th rowspan="2"><center>Jumlah Total</center></th>
                      </tr>
                      <tr>
                        <th><center>Single</center></th>
                        <th><center>Double</center></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <?php foreach ($jml_persediaan as $key) { ?>
                          <tr>

                            <td class="text-center"><?php echo $key->golongan_darah; ?></td>
                            <td class="text-center"><?php echo $key->total_single; ?></td>
                            <td class="text-center"><?php echo $key->total_double; ?></td>
                            <td class="text-center"><?php echo $key->total_semuanya; ?></td>
                          </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div> <!-- close row -->
              


            </div> <!-- close box-body -->
          </div> <!-- close box-success -->
        </div> <!-- close col-md-12 -->
      </div> <!-- close row -->
    </section>
    <!-- /.content -->
  </div>
  
  <?php $this->load->view('template/info-footer.php'); ?>

</div>
<!-- ./wrapper -->

<!-- Tempat meletakkan plugin pada bagian footer -->

<?php $this->load->view('template/footer'); ?>
