<?php $this->load->view('template/header'); ?>

<!-- Tempat meletakkan plugin pada bagian header -->

<?php $this->load->view('template/top-nav'); ?>
<?php $this->load->view('template/side-nav'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
      
      <!-- Main row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title"><strong><i class="fa fa-home"></i>Beranda</strong></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
              HALAMAN BERANDA
            </div> <!-- close box-body -->
          </div> <!-- close box -->
        </div> <!-- close col-md-10 -->
        
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>
  
  <?php $this->load->view('template/info-footer.php'); ?>

</div>
<!-- ./wrapper -->

<!-- Tempat meletakkan plugin pada bagian footer -->

<?php $this->load->view('template/footer'); ?>
