<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Persediaan_model extends CI_Model {

	public function get_persediaan()
	{
		$query = "select 
						general.golongan_darah as golongan_darah, 
						stock_single.total_single_pocket as total_single,
						stock_double.total_double_pocket as total_double,
						general.total_semua as total_semuanya
					from (
							select 
								total_stock.totalstock_id as stock_id,
								type_blood.type_name as golongan_darah, 
								total_stock.total as total_semua 
							from 
								total_stock
							inner join 
								type_blood on (total_stock.type_id = type_blood.type_id)
							inner join
								category_blood on (category_blood.category_id = total_stock.category_id)
							where
								category_blood.category_name = 'Whole Blood'
						) general 
						inner join 
						(
							select
								pocket_stock.total as total_single_pocket, pocket_stock.totalstock_id
							from 
								total_stock
							inner join
								pocket_stock on (total_stock.totalstock_id = pocket_stock.totalstock_id)
							inner join
								pocket on (pocket_stock.pocket_id = pocket.pocket_id)
							inner join
								category_blood on (category_blood.category_id = total_stock.category_id)
							where
								pocket.pocket_name = 'Single' and
								category_blood.category_name = 'Whole Blood'
						) stock_single 
							on (general.stock_id = stock_single.totalstock_id) 
						inner join
						(
							select
								pocket_stock.total as total_double_pocket, pocket_stock.totalstock_id
							from 
								total_stock
							inner join
								pocket_stock on (total_stock.totalstock_id = pocket_stock.totalstock_id)
							inner join
								pocket on (pocket_stock.pocket_id = pocket.pocket_id)
							inner join
								category_blood on (category_blood.category_id = total_stock.category_id)
							where
								pocket.pocket_name = 'Double' and
								category_blood.category_name = 'Whole Blood'
						) stock_double
							on (general.stock_id = stock_double.totalstock_id)
					order by
						general.stock_id;";

		$run = $this->db->query($query);

		return $run->result();
	}

	public function get_persediaan_komponen()
	{
		return $this->db->select('category_name, category_symbol')
						->from('category_blood')
						->get()->row();
	}

	public function get_detail_persediaan($type_name, $pocket_name)
	{
		return $this->db->select("stock_barcode, stock_date_aftap, category_symbol, pocket_name, type_name, stock_rhesus, stock_date_expired, (stock_date_expired::date - now()::date) as umur_darah")
						->from('detail_stock')
						->join('category_blood', 'category_blood.category_id = detail_stock.category_id', 'inner')
						->join('type_blood', 'type_blood.type_id = detail_stock.type_id', 'inner')
						->join('pocket', 'pocket.pocket_id = detail_stock.pocket_id', 'inner')
						->where('pocket.pocket_name', $pocket_name)
						->where('type_blood.type_name', $type_name)
						->order_by('umur_darah, stock_barcode', 'desc')
						->get()->result();
	}

	public function get_count_type_blood($category_symbol, $type_name, $pocket_name)
	{
		return $this->db->select('count(stock_id) as jumlah')
						->from('detail_stock')
						->join('category_blood', 'category_blood.category_id = detail_stock.category_id', 'inner')
						->join('type_blood', 'type_blood.type_id = detail_stock.type_id', 'inner')
						->join('pocket', 'pocket.pocket_id = detail_stock.pocket_id')
						->where('category_symbol', $category_symbol)
						->where('type_name', $type_name)
						->where('pocket_name', $pocket_name)
						->get()
						->row()
						->jumlah;

	}

	/*---------------------- Membuat Data Tiruan Untuk Tabel "detail_stock" ----------------------*/
	public function add_dummy_stock()
	{
		$format 	= 'Y-m-d';
		$aftap_a 	= date($format);
		$expired_a 	= date($format, strtotime("+35 day"));

		$aftap_b	= date($format);
		$expired_b	= date($format, strtotime("+35 day"));

		$aftap_o	= date($format);
		$expired_o	= date($format, strtotime("+35 day"));

		$aftap_ab	= date($format);
		$expired_ab	= date($format, strtotime("+35 day"));

		// $aftap_b	= date($format, strtotime("+1 day"));
		// $expired_b	= date($format, strtotime("+36 day"));	

		$no_barcode_awal = '1622630';

		//golongan darah A tipe kantung Single
		for ($i=1; $i <=150; $i++) { 
			$this->db->trans_begin();

			$no_barcode_awal = $no_barcode_awal + $i;
			$data_dummy = array(
							'stock_barcode'		=> $no_barcode_awal,
							'stock_date_aftap' 	=> $aftap_a,
							'category_id' 		=> '1',
							'pocket_id'			=> '1',
							'type_id'			=> '1',
							'stock_rhesus'		=> 'P',
							'stock_date_expired'=> $expired_a
						);
			$this->db->insert('detail_stock', $data_dummy);
			if ($this->db->trans_status() == FALSE) {
				$this->db->trans_rollback();
			}
			else {
				$this->db->trans_commit();
			}
		}

		//golongan darah A tipe kantung Double
		for ($i=1; $i <=150; $i++) { 
			$this->db->trans_begin();

			$no_barcode_awal = $no_barcode_awal + $i;
			$data_dummy = array(
							'stock_barcode'		=> $no_barcode_awal,
							'stock_date_aftap' 	=> $aftap_a,
							'category_id' 		=> '1',
							'pocket_id'			=> '2',
							'type_id'			=> '1',
							'stock_rhesus'		=> 'P',
							'stock_date_expired'=> $expired_a
						);
			$this->db->insert('detail_stock', $data_dummy);
			if ($this->db->trans_status() == FALSE) {
				$this->db->trans_rollback();
			}
			else {
				$this->db->trans_commit();
			}
		}

		//golongan darah B tipe kantung Single
		for ($i=1; $i <=150; $i++) { 
			$this->db->trans_begin();

			$no_barcode_awal = $no_barcode_awal + $i;
			$data_dummy = array(
							'stock_barcode'		=> $no_barcode_awal,
							'stock_date_aftap' 	=> $aftap_a,
							'category_id' 		=> '1',
							'pocket_id'			=> '1',
							'type_id'			=> '2',
							'stock_rhesus'		=> 'P',
							'stock_date_expired'=> $expired_b
						);
			$this->db->insert('detail_stock', $data_dummy);
			if ($this->db->trans_status() == FALSE) {
				$this->db->trans_rollback();
			}
			else {
				$this->db->trans_commit();
			}
		}

		//golongan darah B tipe kantung Double
		for ($i=1; $i <=150; $i++) { 
			$this->db->trans_begin();

			$no_barcode_awal = $no_barcode_awal + $i;
			$data_dummy = array(
							'stock_barcode'		=> $no_barcode_awal,
							'stock_date_aftap' 	=> $aftap_b,
							'category_id' 		=> '1',
							'pocket_id'			=> '2',
							'type_id'			=> '2',
							'stock_rhesus'		=> 'P',
							'stock_date_expired'=> $expired_b
						);
			$this->db->insert('detail_stock', $data_dummy);
			if ($this->db->trans_status() == FALSE) {
				$this->db->trans_rollback();
			}
			else {
				$this->db->trans_commit();
			}
		}

		//golongan darah O tipe kantung Single
		for ($i=1; $i <=150; $i++) { 
			$this->db->trans_begin();

			$no_barcode_awal = $no_barcode_awal + $i;
			$data_dummy = array(
							'stock_barcode'		=> $no_barcode_awal,
							'stock_date_aftap' 	=> $aftap_o,
							'category_id' 		=> '1',
							'pocket_id'			=> '1',
							'type_id'			=> '3',
							'stock_rhesus'		=> 'P',
							'stock_date_expired'=> $expired_o
						);

			$this->db->insert('detail_stock', $data_dummy);
			if ($this->db->trans_status() == FALSE) {
				$this->db->trans_rollback();
			}
			else {
				$this->db->trans_commit();
			}
		}

		//golongan darah O tipe kantung Double
		for ($i=1; $i <=150; $i++) { 
			$this->db->trans_begin();

			$no_barcode_awal = $no_barcode_awal + $i;
			$data_dummy = array(
							'stock_barcode'		=> $no_barcode_awal,
							'stock_date_aftap' 	=> $aftap_o,
							'category_id' 		=> '1',
							'pocket_id'			=> '2',
							'type_id'			=> '3',
							'stock_rhesus'		=> 'P',
							'stock_date_expired'=> $expired_o
						);
			$this->db->insert('detail_stock', $data_dummy);
			if ($this->db->trans_status() == FALSE) {
				$this->db->trans_rollback();
			}
			else {
				$this->db->trans_commit();
			}
		}

		//golongan darah AB tipe kantung Single
		for ($i=1; $i <=150; $i++) { 
			$this->db->trans_begin();

			$no_barcode_awal = $no_barcode_awal + $i;
			$data_dummy = array(
							'stock_barcode'		=> $no_barcode_awal,
							'stock_date_aftap' 	=> $aftap_ab,
							'category_id' 		=> '1',
							'pocket_id'			=> '1',
							'type_id'			=> '4',
							'stock_rhesus'		=> 'P',
							'stock_date_expired'=> $expired_ab
						);
			$this->db->insert('detail_stock', $data_dummy);
			if ($this->db->trans_status() == FALSE) {
				$this->db->trans_rollback();
			}
			else {
				$this->db->trans_commit();
			}
		}

		//golongan darah AB tipe kantung Double
		for ($i=1; $i <=150; $i++) { 
			$this->db->trans_begin();

			$no_barcode_awal = $no_barcode_awal + $i;
			$data_dummy = array(
							'stock_barcode'		=> $no_barcode_awal,
							'stock_date_aftap' 	=> $aftap_ab,
							'category_id' 		=> '1',
							'pocket_id'			=> '2',
							'type_id'			=> '4',
							'stock_rhesus'		=> 'P',
							'stock_date_expired'=> $expired_ab
						);
			$this->db->insert('detail_stock', $data_dummy);
			if ($this->db->trans_status() == FALSE) {
				$this->db->trans_rollback();
			}
			else {
				$this->db->trans_commit();
			}
		}

	}
	/*--------------------------------------------------------------------------------------------*/

	 function Datatables($dt)
    {
        $search = NULL;
        $columns = '';


        $columns = implode(', ', $dt['col-display']) . ', ' . $dt['id-table'];
        $join = $dt['join'];
        $sql  = "SELECT {$columns} FROM {$dt['table']} {$join}";
        $data = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();
        // pengkondisian aksi seperti next, search dan limit
        $columnd = $dt['col-display'];
        $count_c = count($columnd);
        // search
        $search = $dt['search']['value'];
        /**
         * Search Global
         * pencarian global pada pojok kanan atas
         */
        $where = '';
        if ($search != '') {   
            for ($i=0; $i < $count_c ; $i++) {
                $where .= $columnd[$i] .' LIKE "%'. $search .'%"';
                
                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }
        
        /**
         * Search Individual Kolom
         * pencarian dibawah kolom
         */
        for ($i=0; $i < $count_c; $i++) { 
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                break;
            }
        }
        /**
         * pengecekan Form pencarian
         * pencarian aktif jika ada karakter masuk pada kolom pencarian.
         */
        if ($where != '') {
            $sql .= " WHERE " . $where;
            
        }
        
        // sorting
        $sql .= " ORDER BY {$columnd[$dt['order'][0]['column']]} {$dt['order'][0]['dir']}";
        
        // limit
        $start  = $dt['start'];
        $length = $dt['length'];
        $sql .= " LIMIT {$start}, {$length}";
        
        $list = $this->db->query($sql);
        /**
         * convert to json
         */
        $option['draw']            = $dt['draw'];
        $option['recordsTotal']    = $rowCount;
        $option['recordsFiltered'] = $rowCount;
        $option['data']            = array();
        foreach ($list->result() as $row) {
        /**
         * custom gunakan
         * $option['data'][] = array(
         *                       $row->columnd[0],
         *                       $row->columnd[1],
         *                       $row->columnd[2],
         *                       $row->columnd[3],
         *                       $row->columnd[4],
         *                       .....
         *                     );
         */
           $rows = array();
           for ($i=0; $i < $count_c; $i++) { 
               $rows[] = $row->$columnd[$i];
           }
           $option['data'][] = $rows;
        }
        // eksekusi json
        echo json_encode($option);

        echo $this->db->last_query();
    }

    /*--- Mencoba pagination ---*/

	public function get_data_persediaan()
	{
		return $this->db->select("stock_barcode, stock_date_aftap, category_symbol, pocket_name, type_name, stock_rhesus, stock_date_expired, (stock_date_expired::date - now()::date) as umur_darah")
						->from('detail_stock')
						->join('category_blood', 'category_blood.category_id = detail_stock.category_id', 'inner')
						->join('type_blood', 'type_blood.type_id = detail_stock.type_id', 'inner')
						->join('pocket', 'pocket.pocket_id = detail_stock.pocket_id', 'inner')
						->get()->result();
	}

}

/* End of file Persediaan_model.php */
/* Location: ./application/models/Persediaan_model.php */