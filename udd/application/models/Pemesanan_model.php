<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemesanan_model extends CI_Model {


	/*---------------- GENERATE PERMINTAAN ID ----------------*/
	public function get_counter()
	{
		$query = $this->db->query("select nextval('request_id_seq')");
		return $query->row();
	}

	public function get_nickname($value)
	{
		return $this->db->select('consumer_nickname')
				->from('consumer')
				->where('consumer_name', $value)
				->get()->row();
	}

	public function generate_id($value)
	{
		$counter 	= $this->get_counter();
		$nickname 	= $this->get_nickname($value);

		$temp_counter 	= $counter->nextval;
		$symbol 		= $nickname->consumer_nickname;

		switch (strlen($counter->nextval)) {
			case 1: 
				$value = '000'.$temp_counter; break;
			case 2: 
				$value = '00'.$temp_counter; break;
			case 3: 
				$value = '0'.$temp_counter; break;
			default: 
				$$valu = $temp_counter; break;
		}

		return $symbol.date('Ymd').date('His').$value;
	}	
	/*--------------------------------------------------------*/

	public function get_consumer_id($value)
	{
		$value = $this->db->select('consumer_id')
						->from('consumer')
						->where('consumer_name', $value)
						->get()->row();
						
		return $value->consumer_id;
	}

	public function add_pemesanan($data_pesan, $id_pesan)
	{

		//mengatur data yang akan diinputkan ke tabel request
		$data_request = array(
				'request_id' 			=> $id_pesan,
				'consumer_id' 			=> $this->get_consumer_id($data_pesan['consumer_name']),
				'consumer_employee' 	=> $data_pesan['consumer_employee'],
				'request_date' 			=> date('Y-m-d'),
				'request_time' 			=> date('H:i:s'),
				'request_status' 		=> 'request',
				'request_total' 		=> $data_pesan['total_request']
			);

		//input data ke tabel request
		$input_request = $this->db->insert('request', $data_request);
		
		if ($input_request) {

			//insert data pemesanan golongan darah A
			$data_detail_A = array(
					'request_id' 	=> $id_pesan,
					'category_id' 	=> '1',
					'type_id' 		=> '1',
					'detail_total' 	=> $data_pesan['detail']['type_A']
				);

			$add_detail_A 	= $this->add_detail_request($id_pesan, $data_pesan['category_blood'], 'A',$data_pesan['detail']['type_A']);
			$add_detail_B 	= $this->add_detail_request($id_pesan, $data_pesan['category_blood'], 'B',$data_pesan['detail']['type_B']);
			$add_detail_O 	= $this->add_detail_request($id_pesan, $data_pesan['category_blood'], 'O',$data_pesan['detail']['type_O']);
			$add_detail_AB 	= $this->add_detail_request($id_pesan, $data_pesan['category_blood'], 'AB', $data_pesan['detail']['type_AB']);

			if (($add_detail_A == 'ok') && ($add_detail_B == 'ok') && ($add_detail_O == 'ok') && ($add_detail_AB == 'ok')) {
				return 'ok';
			}
			else {
				return 'cancel';
			}
		}	
		else {
			return 'cancel';
		} //close else "$input_request"

	} /*close function add_pemesanan()*/

	public function add_detail_request($request_id, $category_name, $type_name, $total)
	{
		$type_id 		= $this->get_type_id($type_name);
		$category_id 	= $this->get_category_id($category_name);

		$data_detail = array(
						'request_id' 	=> $request_id,
						'category_id' 	=> $category_id,
						'type_id' 		=> $type_id,
						'detail_total' 	=> $total
					);

		$run_inser_detail = $this->db->insert('detail_request', $data_detail);

		if ($run_inser_detail) {
			return 'ok';
		}
		else {
			return 'cancel';
		}

	}

	/*----------------------------------- Menampilkan Semua Request -----------------------------------*/
	public function show_pemesanan()
	{
		return $this->db->select('request_id, request_date, request_time, consumer_name, consumer_employee, request_total, request_status')
						->from('request')
						->join('consumer', 'consumer.consumer_id = request.consumer_id')
						->where('request_status', 'request')
						->or_where('request_status', 'process')
						->order_by('request_date, request_time', 'asc')
						->get()
						->result();
	}
	/*-------------------------------------------------------------------------------------------------*/

	/*----------------------------------- Melihat Detail Request -----------------------------------*/
	public function detail_request($value) 
	{
		return $this->db->select('request_id, consumer_name, consumer_employee, request_date, request_time, request_status, request_total')
						->from('request')
						->join('consumer', 'consumer.consumer_id = request.consumer_id')
						->where('request.request_id', $value)
						->get()->row();
	}

	public function detail_category($value)
	{
		return $this->db->select('category_name')
						->from('detail_request')
						->join('category_blood', 'category_blood.category_id = detail_request.category_id')
						->where('detail_request.request_id', $value)
						->group_by('category_name')
						->get()->row();
	}

	public function detail_type($value)
	{
		return $this->db->select('type_name, detail_total')
						->from('detail_request')
						->join('request', 'request.request_id = detail_request.request_id')
						->join('type_blood', 'type_blood.type_id = detail_request.type_id')
						->where('request.request_id', $value)
						->get()->result();
	}
	/*----------------------------------------------------------------------------------------------*/

	/*----------------------------------- Mengubah Status Request -----------------------------------*/
	//ambil url consumer berdasarkan request_id
	public function get_url($request_id)
	{
		return $this->db->select('consumer_url')
						->from('consumer')
						->join('request', 'request.consumer_id = consumer.consumer_id')
						->where('request.request_id', $request_id)
						->get()->row();

		/*perintah query :
		select 
			consumer.consumer_url 
		from 
			consumer 
				inner join request on(request.consumer_id = consumer.consumer_id)
		where
			request.request_id = 'rs1201606161340430036';*/
	}

	//mengubah status request
	public function change_status($request_id, $status)
	{
		

		$data = array('request_status' => $status);

		$update = $this->db->set($data)
							->where('request_id', $request_id)
							->update('request', $data);

		if ($update) {
			
			return 'ok';
		}
		else {
			
			return 'cancel';
		}
	}
	/*-----------------------------------------------------------------------------------------------*/

	/*----------------------- Membuat receipt(nota/kwitansi) baru -----------------------*/
	
	//membuat generate receipt_id baru
	public function generate_receipt_id()
	{
		$query_counter = $this->db->query("select nextval('receipt_request_receipt_id_seq')");
		$counter = $query_counter->row();

		switch (strlen($counter->nextval)) {
			case 1: 
				$value = '000'.$counter->nextval; break;
			case 2: 
				$value = '00'.$counter->nextval; break;
			case 3: 
				$value = '0'.$counter->nextval; break;
			default: 
				$value = $counter->nextval; break;
		}

		return date('Ymd').date('His').$value;
	}

	public function create_receipt_pocket($receipt_id, $pocket_id, $total_item)
	{
		$data_insert = array(
							'receipt_id' 			=> $receipt_id,
							'pocket_id'				=> $pocket_id,
							'recpocket_total_item'	=> $total_item,
							'recpocket_status'		=> 'do' 
						);

		$query_insert = $this->db->insert('receipt_pocket', $data_insert);
		if ($data_insert) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	//membuat receipt(nota/kwitansi) untuk menyimpan jumlah item yg dikirim ke BDRS
	public function create_new_receipt($request_id)
	{
		
		
		//membuat berdasarkan receipt(nota/kwitansi) berdasarkan "category_id" dan "type_id"
		$status = 'ok'; 
		for ($i=1; $i <=4; $i++) {
			
			//mengambil generate "receipt_id"
			$receipt_id 	= $this->generate_receipt_id();
			$data_insert 	= array(
								'receipt_id'			=> $receipt_id,
								'category_id' 			=> '1',
								'type_id'				=> $i,
								'receipt_total_send'	=> $this->input->post('jml_kirim_'.$i),
								'request_id'			=> $request_id
							);

			$query_insert = $this->db->insert('receipt_request', $data_insert);
			if ($query_insert) {
				$insert_single = $this->create_receipt_pocket($receipt_id, '1', $this->input->post('jml_single_'.$i));
				$insert_double = $this->create_receipt_pocket($receipt_id, '2', $this->input->post('jml_double_'.$i));
				if (($insert_single == 'ok') && ($insert_double == 'ok')) {
					$status .= $i;
				}
				else {
					
					return 'cancel';
				}
			}
			else {
				
				return 'cancel';
			}
		}
		
		return $status;
	}

	//mengambil "pocket_id" dari tabel "pocket" berdasarkan "pocket_name"
	public function get_pocket_id($pocket_name)
	{
		$query = $this->db->select('pocket_id')
						->from('pocket')
						->where('pocket_name', $pocket_name)
						->get()->row();

		return $query->pocket_id;
	}

	//mengambil jumlah total stock pada tabel "total_stock" berdasarkan "category_id" dan "type_id"
	public function get_jml_total_stock($category_id, $type_id)
	{
		$query = $this->db->select('total')
						->from('total_stock')
						->where('category_id', $category_id)
						->where('type_id', $type_id)
						->get()->row();

		return $query->total;
	}

	//mengambil jumlah stock perkantung dari tabel "pocket_stock" berdasarkan "pocket_name" dan "totalstock_id"
	public function get_jml_pocket_stock($pocket_name, $totalstock_id)
	{
		$pocket_id = $this->get_pocket_id($pocket_name);

		$query = $this->db->select('total')
						->from('pocket_stock')
						->where('pocket_id', $pocket_id)
						->where('totalstock_id', $totalstock_id)
						->get()->row();

		return $query->total;
	}

	//mengurangi persediaan pada tabel "total_stock" berdasarkan "category_id", "type_id" dan "jumlah yg diambil"
	public function update_jml_total_stock($category_id, $type_id, $jumlah)
	{
		$jml_stock = $this->get_jml_total_stock($category_id, $type_id);
		$upd_stock = $jml_stock - $jumlah;

		$value 		= array('total' => $upd_stock);
		$kondisi	= array('category_id' => $category_id, 'type_id' => $type_id);
		
		$update_data = $this->db->where($kondisi)
								->update('total_stock', $value);
		if ($update_data) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	//mengurangi persediaan pada tabel "pocket_stock" berdasarkan "category_id", "type_id", "pocket_name", dan "jumlah yg diambil"
	public function update_jml_pocket_stock($category_id, $type_id, $pocket_name,$jumlah)
	{
		$total_stock_id = $this->get_totalstock_id($category_id, $type_id);
		$jml_stock		= $this->get_jml_pocket_stock($pocket_name, $total_stock_id);
		$pocket_id 		= $this->get_pocket_id($pocket_name);

		$upd_stock	= $jml_stock - $jumlah;
		$value 		= array('total' => $upd_stock);
		$kondisi	= array('pocket_id' => $pocket_id, 'totalstock_id' => $total_stock_id);

		$update_data = $this->db->where($kondisi)
								->update('pocket_stock', $value);
		if ($update_data) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	public function update_stock()
	{
		

		$flag = 1;
		for ($i=1; $i <=4; $i++) { 
			
			$jml_single_in			= $this->input->post('jml_single_'.$i);
			$jml_double_in			= $this->input->post('jml_double_'.$i);
			$new_total_stock 		= $this->input->post('jml_single_'.$i) + $this->input->post('jml_double_'.$i);
			
			$update_jml_total_stock = $this->update_jml_total_stock('1', $i, $new_total_stock);

			if ($update_jml_total_stock == 'ok') {
				$update_single = $this->update_jml_pocket_stock('1', $i, 'Single', $jml_single_in);
				$update_double = $this->update_jml_pocket_stock('1', $i, 'Double', $jml_double_in);

				if (($update_single=='ok') && ($update_double=='ok')) {
					
					$flag++;
				}
				else {
					// break;
					
					return 'cancel';
				}
			}
			else {
				// break;
				
				return 'cancel';
			}

		}

		return 'ok'.$flag;
	}

	/*----------------------------------------------------------------------------------*/

	/*------------------------------ Membatalkan Request (Service) ------------------------------*/
	public function delete_request($request_id)
	{
		$data = array('request_id' => $request_id);

		

		$detele = $this->db->delete('request', $data);

		if ($this->db->trans_status() == FALSE) {
			
			return 'cancel';
		}
		else {
			
			return 'ok';
		}
	}
	/*-----------------------------------------------------------------------------------------*/

	/*----------------------- Mengubah Data Request (Service) -----------------------*/
	public function get_category_id($category_name)
	{
		$query =  $this->db->select('category_id')
						->from('category_blood')
						->where('category_name', $category_name)
						->get()->row();

		return $query->category_id;
	}

	public function update_request($data_request)
	{
		

		//update data pada tabel request
		$data_update = array('request_total' => $data_request['total_request']);
		$update_request = $this->db->set($data_update)
								->where('request_id', $data_request['request_id'])
								->update('request', $data_update);

		//update data pada tabel detail_request

		//mengambil category_id
		$category = $this->get_category_id($data_request['category_name']);
		
		//proses update data
		$condition = array('request_id' => $data_request['request_id'], 'category_id' => $category, 'type_id' => '1');

		$detail_update = array('detail_total' => $data_request['detail_update']['type_A']);

		$update = $this->db->set($detail_update)
						->where($condition)
						->update('detail_request', $detail_update);
		
		//jika gagal
		if ($this->db->trans_status() == FALSE) {
			
			return 'cancel';
		}
		//jika berhasil
		else {
			$condition 	= array('request_id' => $data_request['request_id'], 'category_id' => $category, 'type_id' => '2');

			$detail_update = array('detail_total' => $data_request['detail_update']['type_B']);

			$update = $this->db->set($detail_update)
							->where($condition)
							->update('detail_request', $detail_update);

			if ($this->db->trans_status() == FALSE) {
				$tthis->db->trans_rollback();
				return 'cancel';
			}
			else {
				$condition 	= array('request_id' => $data_request['request_id'], 'category_id' => $category, 'type_id' => '3');

				$detail_update = array('detail_total' => $data_request['detail_update']['type_O']);

				$update = $this->db->set($detail_update)
								->where($condition)
								->update('detail_request', $detail_update);

				if ($this->db->trans_status() == FALSE) {
					
					return 'cancel';
				}
				else {
					$condition 	= array('request_id' => $data_request['request_id'], 'category_id' => $category, 'type_id' => '4');

					$detail_update = array('detail_total' => $data_request['detail_update']['type_AB']);

					$update = $this->db->set($detail_update)
									->where($condition)
									->update('detail_request', $detail_update);
					
					if ($this->db->trans_status() == FALSE) {
						
						return 'cancel';
					}
					else {
						
						return 'ok';
					}
				}
			}
		}
		
	}
	/*---------------------------------------------------------------------------------------------*/

	/*---------------------------------- Pengiriman Data Darah ----------------------------------*/
	//mengambil jumlah permintaan darah berdasarkan "request_id"
	public function get_request_data($request_id)
	{
		return $this->db->select('type_name, detail_total, type_blood.type_id as id_golongan')
						->from('detail_request')
						->join('type_blood', 'type_blood.type_id = detail_request.type_id')
						->where('request_id', $request_id)
						->get()->result();
	}

	//mengambil data darah dari tabel "detail_stock" bersasarkan jumlah permintaan, golongan darah & tipe kantung
	public function get_data_detail_stock($category_name, $type_name, $pocket_name, $jumlah)
	{
		return $this->db->select('stock_id, stock_barcode, stock_date_aftap, detail_stock.category_id, category_name, category_symbol, detail_stock.pocket_id, pocket_name, detail_stock.type_id, type_name, stock_rhesus, stock_date_expired')
						->from('detail_stock')
						->join('type_blood', 'type_blood.type_id = detail_stock.type_id')
						->join('pocket', 'pocket.pocket_id = detail_stock.pocket_id')
						->join('category_blood', 'category_blood.category_id = detail_stock.category_id')
						->where('pocket.pocket_name', $pocket_name)
						->where('category_blood.category_name', $category_name)
						->where('type_blood.type_name', $type_name)
						->order_by('(stock_date_expired::date)', 'desc')
						->order_by('stock_id', 'ASC')
						->limit($jumlah)
						->get()->result();
	}

	//mengambil totalstock_id dari tabel "total_stock" berdasarkan category_id dan type_id
	public function get_totalstock_id($category_id, $type_id)
	{
		$query = $this->db->select('totalstock_id')
						->from('total_stock')
						->where('category_id', $category_id)
						->where('type_id', $type_id)
						->get()->row();

		return $query->totalstock_id;
	}

	//mengambil "total_request" dari tabel "request" berdasarkan "request_id"
	public function get_total_request($request_id)
	{
		$query = $this->db->select('request_total')
						->from('request')
						->where('request_id', $request_id)
						->get()->row();

		return $query->request_total;
	}

	//mengambil exit_id dari tabel "exit_blood" berdasarkan "request_id", "category_name" dan "type_name"
	public function get_exit_id_exit_stock($request_id, $category_name, $type_name)
	{
		$query = $this->db->select('exit_id')
						->from('exit_blood')
						->join('category_blood', 'exit_blood.category_id = category_blood.category_id', 'inner')
						->join('type_blood', 'exit_blood.type_id = type_blood.type_id', 'inner')
						->where('request_id', $request_id)
						->where('category_name', $category_name)
						->where('type_name', $type_name)
						->where('exit_date', date('Y-m-d'))
						->get()->row();

		if (count($query) == 0) {
			return "1";
		}
		else {
			return $query->exit_id;
		}
	}

	//mengecek adanya log darah keluar dari tabel "exit_blood" berdasarkan "request_id", "category_name", dan "type_name" 
	public function check_exit_blood($request_id, $category_name, $type_name)
	{
		$query = $this->db->select('count(exit_id) as jumlah')
						->from('exit_blood')
						->join('category_blood','category_blood.category_id = exit_blood.category_id')
						->join('type_blood', 'type_blood.type_id = exit_blood.type_id')
						->where('request_id', $request_id)
						->where('category_name', $category_name)
						->where('type_name', $type_name)
						->where('exit_date', date('Y-m-d'))
						->get()->row();

		$exist = $query->jumlah;
		if ($exist == 1) {
			return 'ada';
		}
		else if ($exist == 0) {
			return 'kosong';
		}
		else {
			return 'ada kesalahan';
		}
	}

	//mengecek adanya log darah keluar perkantung dari tabel "exit_pocket" berdasarkan "request_id", "category_name", "type_name", dan "pocket_name" 
	public function check_exit_pocket($request_id, $category_name, $type_name, $pocket_name)
	{
		$get_exit_id = $this->get_exit_id_exit_stock($request_id, $category_name, $type_name);

		$query = $this->db->select('count(exitpocket_id) as jumlah')
						->from('exit_pocket')
						->join('pocket', 'pocket.pocket_id = exit_pocket.pocket_id')
						->join('exit_blood', 'exit_blood.exit_id = exit_pocket.exit_id')
						->where('pocket_name', $pocket_name)
						->where('exit_date', date('Y-m-d'))
						->where('exit_pocket.exit_id', $get_exit_id)
						->get()->row();
		
		$exist = $query->jumlah;
		if ($exist == '1') {
			return 'ada';
		}
		else if ($exist == '0') {
			return 'kosong';
		}
		else {
			return 'ada kesalahan';
		}
	}

	public function get_type_id($type_name)
	{
		$query =  $this->db->select('type_id')
						->from('type_blood')
						->where('type_name', $type_name)
						->get()->row();

		return $query->type_id;
	}

	//membuat log darah keluar baru pada tabel "exit_blood" berdasarkan "request_id", "category_name", "type_name"
	public function create_new_exit_blood($request_id, $category_name, $type_name, $jumlah)
	{
		$category_id = $this->get_category_id($category_name);
		$type_id 	 = $this->get_type_id($type_name);

		$data_insert = array(
							'category_id'	=> $category_id,
							'type_id'		=> $type_id,
							'exit_date'		=> date('Y-m-d'),
							'exit_total'	=> $jumlah,
							'request_id'	=> $request_id
						);

		$new_log = $this->db->insert('exit_blood', $data_insert);
		if ($new_log) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	//membuat log darah keluar perkantung baru pada tabel "exit_pocket" berdasarkan "request_id", "category_name", "type_name", dan "pocket_name"
	public function create_new_exit_pocket($request_id, $category_name, $type_name, $pocket_name, $jumlah)
	{
		$exit_id 	= $this->get_exit_id_exit_stock($request_id, $category_name, $type_name);
		$pocket_id 	= $this->get_pocket_id($pocket_name);

		$data_insert = array(
							'exit_id'			=> $exit_id,
							'pocket_id'			=> $pocket_id,
							'exitpocket_total' 	=> $jumlah
						);

		$new_log = $this->db->insert('exit_pocket', $data_insert);
		if ($new_log) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	//mengambil "exit_total" dari tabel "exit_blood" berdasarkan "request_id", "category_name", dan "type_name"
	public function get_exit_total_exit_blood($request_id, $category_name, $type_name)
	{
		$query = $this->db->select('exit_total')
						->from('exit_blood')
						->join('category_blood', 'category_blood.category_id = exit_blood.category_id')
						->join('type_blood', 'type_blood.type_id = exit_blood.type_id')
						->where('category_name', $category_name)
						->where('type_name', $type_name)
						->where('exit_date', date('Y-m-d'))
						->where('request_id', $request_id)
						->get()->row();

		return $query->exit_total;
	}

	//mengambil "pockettotal_exit" dari tabel "exit_pocket" berdasarkan "request_id", "category_name", "type_name", dan "pocket_name"
	public function get_exit_pockettotal_exit_pocket($request_id, $category_name, $type_name, $pocket_name)
	{
		$query = $this->db->select('exit_pocket.exitpocket_total as total')
						->from('exit_pocket')
						->join('pocket', 'pocket.pocket_id = exit_pocket.pocket_id', 'inner')
						->join('exit_blood', 'exit_blood.exit_id = exit_pocket.exit_id', 'inner')
						->join('category_blood', 'category_blood.category_id = exit_blood.category_id', 'inner')
						->join('type_blood', 'type_blood.type_id = exit_blood.type_id', 'inner')
						->where('request_id', $request_id)
						->where('pocket_name', $pocket_name)
						->where('type_name', $type_name)
						->where('category_name', $category_name)
						->get()->row();

		return $query->total;
	}

	//update jumlah "exit_total" pada "exit_blood" berdasarkan "request_id", "category_name", dan "type_name"
	public function update_exit_total_exit_blood($request_id, $category_name, $type_name)
	{
		$get_exit_total 	= $this->get_exit_total_exit_blood($request_id, $category_name, $type_name);
		
		$final_exit_total 	= $get_exit_total + 1;
		$get_category_id	= $this->get_category_id($category_name);
		$get_type_id		= $this->get_type_id($type_name);
		
		$kondisi = array(
						'request_id'	=> $request_id,
						'category_id' 	=> $get_category_id,
						'type_id'		=> $get_type_id,
						'exit_date'		=> date('Y-m-d')
					);

		$data_update = array('exit_total' => $final_exit_total);

		$update_exit_toal = $this->db->where($kondisi)
									->update('exit_blood', $data_update);

		if ($update_exit_toal) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	//update jumlah "exit_pockettotal" pada "exit_pocket" berdasarkan "request_id", "category_name", "type_name" dan "pocket_name"
	public function update_exit_pockettotal_exit_pocket($request_id, $category_name, $type_name, $pocket_name)
	{
		$get_exit_id 			= $this->get_exit_id_exit_stock($request_id, $category_name, $type_name);
		$get_exit_pocket_total 	= $this->get_exit_pockettotal_exit_pocket($request_id, $category_name, $type_name, $pocket_name);

		$final_exit_pocket_total = $get_exit_pocket_total + 1;
		$get_pocket_id 			 = $this->get_pocket_id($pocket_name);

		$kondisi = array(
						'exit_id' 	=> $get_exit_id,
						'pocket_id'	=> $get_pocket_id
					); 

		$data_update = array('exitpocket_total' => $final_exit_pocket_total);

		$update_exit_pockettotal = $this->db->where($kondisi)
											->update('exit_pocket', $data_update);

		if ($update_exit_pockettotal) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	//menghapus data pada tabel "detail_stock" berdasarkan "stock_id"
	public function delete_data_detail_stock($stock_id)
	{
		$data_delete = array('stock_id' => $stock_id);

		$delete_action = $this->db->delete('detail_stock', $data_delete);

		if ($delete_action) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	//menambahkan "data_darah" pada tabel "detail_blood_exit" 
	public function add_data_detail_blood_exit($data_darah)
	{
		$add_data = $this->db->insert('detail_blood_exit', $data_darah);
		if ($add_data) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	//mengambil "receipt_id" dari tabel "receipt_request"
	public function get_receipt_id_receipt_request($request_id, $category_name, $type_name)
	{
		$query = $this->db->select('receipt_id')
						->from('receipt_request')
						->join('category_blood', 'category_blood.category_id = receipt_request.category_id', 'inner')
						->join('type_blood', 'type_blood.type_id = receipt_request.type_id')
						->where('request_id', $request_id)
						->where('category_name', $category_name)
						->where('type_name', $type_name)
						->get()->row();

		// return $query->receipt_id;

		if (empty($query)) {
			return 'cancel';
		}
		else {
			return $query->receipt_id;
		}
	}

	//mengupdate status pada tabel "receipt_pocket"
	public function update_recpocket_status_receipt_pocket($request_id, $category_name, $type_name, $pocket_name)
	{
		$get_pocket_id 	= $this->get_pocket_id($pocket_name);
		$get_receipt_id = $this->get_receipt_id_receipt_request($request_id, $category_name, $type_name);

		$data_update 	= array('recpocket_status' => 'done');
		$kondisi		= array(
							'receipt_id' => $get_receipt_id,
							'pocket_id'	 => $get_pocket_id
						);

		$update_data	= $this->db->where($kondisi)
								->update('receipt_pocket', $data_update);
		
		if ($update_data) {
		 	return 'ok';
		}
		else {
			return 'cancel';
		} 
	}
	/*-------------------------------------------------------------------------------------------*/

	/*---------------------- Mengkonfirmasi Pemesanan (Service) ----------------------*/
	public function konfirm_request($request_id)
	{
		$data_update_request = array(
						'request_status' => 'done'
					);

		$data_update_request_shipping = array(
						'reship_date_konfirm' 	=> date('Y-m-d'),
						'reship_time_konfirm' 	=> date('H:i:s'),
						'reship_konfirm'		=> 'y',
					);

		$run_update_request = $this->db->where('request_id', $request_id)
									->update('request', $data_update_request);

		$run_update_request_shipping = $this->db->where('request_id', $request_id)
												->update('request_shipping', $data_update_request_shipping);

		if (($run_update_request) && ($run_update_request_shipping)) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}
	/*--------------------------------------------------------------------------------*/

	/*---------------------- Menngambil Jumlah Pemesanan Darah yang Masuk ----------------------*/
	public function count_pemesanan_masuk()
	{
		return $this->db->select('count(request_id) as jumlah_masuk')
						->from('request')
						->where('request_status', 'request')
						->or_where('request_status', 'process')
						->get()->row();
	}
	/*------------------------------------------------------------------------------------------*/

	/*---------------------- Mengambil Jumlah Pemesanan Darah yang Terkirim & Menunggu Konfirmasi ----------------------*/
	public function count_pemesanan_keluar($value='')
	{
		return $this->db->select('count(request_id) as jumlah_keluar')
						->from('request')
						->where('request_status', 'sent')
						->get()->row();
	}
	/*-------------------------------------------------------------------------------------------------------------------*/
	
	/*---------------------- Menampilkan Pemesanan Darah yang Terkirim & Menunggu Konfirmasi ----------------------*/
	public function get_pemesanan_terkirim()
	{	
		return $this->db->select('request_id, request_date, request_time, consumer_name, consumer_employee, request_total, request_status')
						->from('request')
						->join('consumer', 'consumer.consumer_id = request.consumer_id')
						->where('request_status', 'sent')
						->order_by('request_date, request_time', 'asc')
						->get()
						->result();
	}
	/*-------------------------------------------------------------------------------------------------------------*/

	/*---------------------- Menampilkan Pemesanan Darah yang Sudah Terkirim & Terkonfirmasi ----------------------*/
	public function get_pemesanan_selesai()
	{	
		return $this->db->select('request_id, request_date, request_time, consumer_name, consumer_employee, request_total, request_status')
						->from('request')
						->join('consumer', 'consumer.consumer_id = request.consumer_id')
						->where('request_status', 'done')
						->order_by('request_date, request_time', 'asc')
						->get()
						->result();
	}
	/*-------------------------------------------------------------------------------------------------------------*/

	/*---------------------- Menampilkan Detail Data Darah yang Terkirim & Belum Terkonfirmasi  ----------------------*/
	public function detail_sent($request_id)
	{
		$query = "select 
						general.exit_id as exit_id2,
						general.golongan_darah as golongan_darah, 
						sent_single.total_single_pocket as total_single,
						sent_double.total_double_pocket as total_double,
						general.total_semua as total_kirim,
						(sent_single.total_single_pocket + (sent_double.total_double_pocket * 2)) as jumlah_total
					from (
							select 
								exit_blood.exit_id as exit_id,
								exit_blood.request_id as request_id,
								type_blood.type_name as golongan_darah, 
								exit_blood.exit_total as total_semua 
							from 
								exit_blood
							inner join 
								type_blood on (exit_blood.type_id = type_blood.type_id)
							inner join
								category_blood on (category_blood.category_id = exit_blood.category_id)
							where
								category_blood.category_name = 'Whole Blood' and
								request_id 					 = '$request_id'
							order by request_id
						) general 
							inner join 
						(
							select
								exit_blood.exit_id as exit_id,
								exit_blood.request_id as request_id,
								exit_pocket.exitpocket_total as total_single_pocket
							from 
								exit_pocket
							inner join
								pocket on (exit_pocket.pocket_id = pocket.pocket_id)
							inner join
								exit_blood on (exit_blood.exit_id = exit_pocket.exit_id)
							inner join
								category_blood on (category_blood.category_id = exit_blood.category_id)
							where
								pocket.pocket_name 				= 'Single' and
								category_blood.category_name 	= 'Whole Blood' and
								exit_blood.request_id 			= '$request_id'
							order by exit_id
						) sent_single 
								on (general.exit_id = sent_single.exit_id)
							inner join
						(
							select
								exit_blood.exit_id as exit_id,
								exit_blood.request_id as request_id,
								exit_pocket.exitpocket_total as total_double_pocket
							from 
								exit_pocket
							inner join
								pocket on (exit_pocket.pocket_id = pocket.pocket_id)
							inner join
								exit_blood on (exit_blood.exit_id = exit_pocket.exit_id)
							inner join
								category_blood on (category_blood.category_id = exit_blood.category_id)
							where
								pocket.pocket_name 				= 'Double' and
								category_blood.category_name 	= 'Whole Blood' and
								exit_blood.request_id 			= '$request_id'
							order by exit_id
						) sent_double
								on (general.exit_id = sent_double.exit_id)
						group by
							golongan_darah, total_single, total_double, total_kirim, exit_id2, jumlah_total
						order by
							exit_id2
					;";

		$run = $this->db->query($query);

		return $run->result();

	}
	/*-----------------------------------------------------------------------------------------------------------------*/

	/*----------------- Menambahkan pemesanan ke tabel pengiriman (Service) -----------------*/
	public function add_request_shipping($request_id)
	{
		$data_insert = array(
				'request_id'			=> $request_id,
				'reship_complete' 		=> 'none',
			);

		$run_insert = $this->db->insert('request_shipping', $data_insert);

		if ($run_insert) {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}
	/*---------------------------------------------------------------------------------------*/

	public function check_status_pemesanan($request_id)
	{
		$query = $this->db->select('request_status')
						->from('request')
						->where('request_id', $request_id)
						->get()
						->row()
						->request_status;

		if ($query == 'request') {
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}
	
}

/* End of file Pemesanan_model.php */
/* Location: ./application/models/Pemesanan_model.php */